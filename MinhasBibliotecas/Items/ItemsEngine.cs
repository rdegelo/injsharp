﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using uoNet;

namespace MinhasBibliotecas.Items
{
    public class ItemsEngine : BaseClass
    {
        public ItemsEngine(UO uo, IEnumerable<UOItem> items)
            : base(uo)
        {
            this.UO = uo;
            this.Items = items;
        }

        public ItemsEngine(UO uo)
            : base(uo)
        {
            this.UO = uo;

            HashSet<UO.FoundItem> itens = new HashSet<UO.FoundItem>();
            List<UOItem> uoItens = new List<UOItem>();

            for (int i = 0; i < UO.ScanItems(true) - 1; i++)
            {
                var item = UO.GetItem(i);
                itens.Add(item);
                uoItens.Add(new UOItem(UO, this, item, true));
            }

            for (int i = 0; i < UO.ScanItems(false) - 1; i++)
            {
                var item = UO.GetItem(i);
                if (!itens.Contains(item))
                {
                    itens.Add(item);
                    uoItens.Add(new UOItem(UO, this, item, false));
                }
            }

            Items = uoItens;
        }

        public UO UO { get; private set; }
        public IEnumerable<UOItem> Items { get; private set; }

        public void RefreshItens()
        {
            foreach (var item in Items)
                item.Refresh();
        }
    }

    public static class ItemsEngineExtentions
    {
        public static ItemsEngine WithName(this ItemsEngine engine, string name)
        {
            Regex regex = new Regex(".*" + name + ".*", RegexOptions.IgnoreCase);

            return new ItemsEngine(engine.UO, engine.Items.Where(o => regex.IsMatch(o.Properties.Name)));
        }

        public static ItemsEngine Equipped(this ItemsEngine engine)
        {
            return engine.InContainer(engine.UO.CharID, false);
        }

        public static ItemsEngine InBackPack(this ItemsEngine engine, bool recursive)
        {
            return engine.InContainer(engine.UO.BackpackID, recursive);
        }

        public static ItemsEngine InContainerType(this ItemsEngine engine, int container_type, bool recursive)
        {
            return new ItemsEngine(engine.UO, engine.Items.Where(o => IsParentType(o, container_type, recursive)));
        }

        public static ItemsEngine InContainer(this ItemsEngine engine, int container_id, bool recursive)
        {
            return new ItemsEngine(engine.UO, engine.Items.Where(o => IsParent(o, container_id, recursive)));
        }

        private static bool IsParent(UOItem item, int container_id, bool recursive)
        {
            if (item.Item.Kind != 0)
                return false;
            if (item.Parent != null && item.Parent.Item.ID == container_id)
                return true;
            else if (recursive && item.Parent != null)
                return IsParent(item.Parent, container_id, recursive);

            return false;
        }

        private static bool IsParentType(UOItem item, int container_type, bool recursive)
        {
            if (item.Item.Kind != 0)
                return false;
            if (item.Parent.Item.Type == container_type)
                return true;
            else if (recursive)
                return IsParent(item.Parent, container_type, recursive);

            return false;
        }

        public static ItemsEngine WithID(this ItemsEngine engine, int id)
        {
            return new ItemsEngine(engine.UO, engine.Items.Where(o => o.Item.ID == id));
        }

        public static ItemsEngine WithKind(this ItemsEngine engine, int kind)
        {
            return new ItemsEngine(engine.UO, engine.Items.Where(o => o.Item.Kind == kind));
        }

        public static ItemsEngine WithColor(this ItemsEngine engine, int color)
        {
            return new ItemsEngine(engine.UO, engine.Items.Where(o => o.Item.Col == color));
        }

        public static ItemsEngine WithType(this ItemsEngine engine, int type)
        {
            return new ItemsEngine(engine.UO, engine.Items.Where(o => o.Item.Type == type));
        }

        public static ItemsEngine Visible(this ItemsEngine engine, bool state)
        {
            return new ItemsEngine(engine.UO, engine.Items.Where(o => o.Visible == state));
        }

        public static ItemsEngine InRange(this ItemsEngine engine, int range)
        {
            return new ItemsEngine(engine.UO, engine.Items.Where(o => o.Dist <= range));
        }

        public static ItemsEngine OnGround(this ItemsEngine engine)
        {
            return engine.WithKind(1);
        }

        public static ItemsEngine InAnyContainer(this ItemsEngine engine)
        {
            return engine.WithKind(0);
        }        
    }

    public class UOItem : BaseClass
    {
        public UOItem(UO uo, ItemsEngine rootEngine, UO.FoundItem item, bool visible)
            : base(uo)
        {
            this.Item = item;
            this.RootEngine = rootEngine;
            this.Visible = visible;
        }

        public ItemsEngine RootEngine { get; private set; }
        public bool Visible { get; private set; }
        public UO.FoundItem Item { get; private set; }

        private UOItem _parent;
        public UOItem Parent 
        {
            get
            {
                if (_parent == null && Item.Kind == 0)
                    _parent = RootEngine.Items.SingleOrDefault(o => o.Item.ID == Item.ContID);

                return _parent;
            }
        }

        public int Dist 
        {
            get
            {
                return Math.Max(Math.Abs(UO.CharPosX - Item.X), Math.Abs(UO.CharPosY - Item.Y));
            }
        }

        private UO.PropertyInfo _properties;
        public UO.PropertyInfo Properties 
        {
            get 
            {
                if (_properties == null)
                    _properties = UO.Property(Item.ID);

                return _properties; 
            }
        }

        public void Refresh()
        {
            Item = UO.GetItem(Item.ID);
            _properties = null;
            _parent = null;
        }

        public override string ToString()
        {
            return string.Format("{0}: {1}", Item.ID, Properties.Name);
        }
    }
}
