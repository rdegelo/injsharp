﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using uoNet;

namespace MinhasBibliotecas
{
    public class BaseClass
    {
        public UO UO { get; private set; }

        public BaseClass(UO uo)
        {
            this.UO = uo;
        }
    }
}
