﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace MinhasBibliotecas.OCR
{
    class ImageReader
    {
        [DllImport("AspriseOCR.dll", EntryPoint = "OCR")]
        public static extern IntPtr OCR(string file, int type);

        [DllImport("AspriseOCR.dll", EntryPoint = "OCRpart")]
        static extern IntPtr OCRpart(string file, int type, int startX, int startY, int width, int height);

        [DllImport("AspriseOCR.dll", EntryPoint = "OCRBarCodes")]
        static extern IntPtr OCRBarCodes(string file, int type);

        [DllImport("AspriseOCR.dll", EntryPoint = "OCRpartBarCodes")]
        static extern IntPtr OCRpartBarCodes(string file, int type, int startX, int startY, int width, int height);

        //int startX = 0;
        //    int startY = 0;
        //    int width = -1;
        //    int height = -1;
        //    try
        //    {
        //        startX = Convert.ToInt32(textStartX.Text);
        //        startY = Convert.ToInt32(textStartY.Text);
        //        width = Convert.ToInt32(textWidth.Text);
        //        height = Convert.ToInt32(textHeight.Text);
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex.StackTrace);
        //    }
        //    String result = Marshal.PtrToStringAnsi(OCRpart(textImage.Text, -1, startX, startY, width, height));
        //    textResults.Text = result;
    }
}
