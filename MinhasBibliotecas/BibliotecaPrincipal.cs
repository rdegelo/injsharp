﻿using MinhasBibliotecas.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using uoNet;

namespace MinhasBibliotecas
{
    public class BibliotecaPrincipal : BaseClass
    {
        public BibliotecaPrincipal(UO uo)
            : base(uo)
        {
        
        }

        public Items.ItemsEngine WorldItems 
        {
            get 
            {
                return new ItemsEngine(UO);
            }
        }
    }
}
