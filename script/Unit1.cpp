////////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2001 mamaich
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
////////////////////////////////////////////////////////////////////////////////


#using <System.Windows.Forms.dll>
#using <System.dll>

using namespace InjSharp;
using namespace System;
using namespace System::Reflection;
using namespace System::IO;

#include "Stdafx.h"
#include "extdll.h"
#include "mycsubs.h"
#include "InjInterop.cpp"
#include <vcclr.h>
#include <direct.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#include <Windows.h>
#pragma hdrstop

//#pragma argsused
int WINAPI DllEntryPoint(HINSTANCE hinst, unsigned long reason, void* lpReserved)
{
    return 1;
}
//---------------------------------------------------------------------------

public ref class Globals abstract sealed
{
public:
	static ScriptConnector^ scriptConnector;
	static HWND UOWindow = 0;

	static void Initialize(DllInterface *intf)
	{
		//System::Diagnostics::Debugger::Launch();

		Globals::scriptConnector = gcnew ScriptConnector();
		Globals::scriptConnector->UO=intf;
		Globals::scriptConnector->Functions = new TList<CFuncTable>();

		void* parser = System::Runtime::InteropServices::GCHandle::ToIntPtr(System::Runtime::InteropServices::GCHandle::Alloc(Globals::scriptConnector)).ToPointer();	

		Globals::scriptConnector->UO->AddClasses((ParserObject*)parser, GetLibraryFunctions());

		InjConnector^ connector = gcnew InjConnector();
		connector->Function = gcnew System::Func<System::String^, array<System::Object^>^, System::Int32, System::Object^>(Globals::scriptConnector, &ScriptConnector::CallFunction);    
		connector->Print = gcnew System::Action<System::String^>(Globals::scriptConnector, &ScriptConnector::CallPrint);
		connector->ReloadFunctions = gcnew System::Action(Globals::scriptConnector, &ScriptConnector::ReloadFunctions);
		connector->GetFunctions = gcnew System::Func<System::Collections::Generic::List<System::String^>^>(Globals::scriptConnector, &ScriptConnector::GetFunctions);
		connector->DoCommand = gcnew System::Action<System::String^>(Globals::scriptConnector, &ScriptConnector::DoCommand);

		connector->Attributes->Life = Globals::scriptConnector->UO->Life;
		connector->Attributes->STR = Globals::scriptConnector->UO->STR;
		connector->Attributes->Mana = Globals::scriptConnector->UO->Mana;
		connector->Attributes->INT = Globals::scriptConnector->UO->INT;
		connector->Attributes->Stamina = Globals::scriptConnector->UO->Stamina;
		connector->Attributes->DEX = Globals::scriptConnector->UO->DEX;
		connector->Attributes->Armor = Globals::scriptConnector->UO->Armor;
		connector->Attributes->Weight = Globals::scriptConnector->UO->Weight;
		connector->Attributes->Gold = Globals::scriptConnector->UO->Gold;
		connector->Attributes->BM = Globals::scriptConnector->UO->BM;
		connector->Attributes->BP = Globals::scriptConnector->UO->BP;
		connector->Attributes->GA = Globals::scriptConnector->UO->GA;
		connector->Attributes->GS = Globals::scriptConnector->UO->GS;
		connector->Attributes->MR = Globals::scriptConnector->UO->MR;
		connector->Attributes->NS = Globals::scriptConnector->UO->NS;
		connector->Attributes->SA = Globals::scriptConnector->UO->SA;
		connector->Attributes->SS = Globals::scriptConnector->UO->SS;
		connector->Attributes->VA = Globals::scriptConnector->UO->VA;
		connector->Attributes->EN = Globals::scriptConnector->UO->EN;
		connector->Attributes->WH = Globals::scriptConnector->UO->WH;
		connector->Attributes->FD = Globals::scriptConnector->UO->FD;
		connector->Attributes->BR = Globals::scriptConnector->UO->BR;
		connector->Attributes->H = Globals::scriptConnector->UO->H;
		connector->Attributes->C = Globals::scriptConnector->UO->C;
		connector->Attributes->M = Globals::scriptConnector->UO->M;
		connector->Attributes->L = Globals::scriptConnector->UO->L;
		connector->Attributes->B = Globals::scriptConnector->UO->B;
		connector->Attributes->AR = Globals::scriptConnector->UO->AR;
		connector->Attributes->BT = Globals::scriptConnector->UO->BT;

		GetUOWindow();

		connector->UOWindowHandle = System::IntPtr(UOWindow);
		connector->ParentWindowHandle = System::IntPtr(Globals::scriptConnector->UO->Window);

		Globals::scriptConnector->MainWindow = gcnew MainForm(connector);
		Globals::scriptConnector->MainWindow->Visible = true;
		Globals::scriptConnector->MainWindow->Left = 0;
		Globals::scriptConnector->MainWindow->Top = 0;

		//ResizeWindow();

		SetParent((HWND)Globals::scriptConnector->MainWindow->Handle.ToPointer(), Globals::scriptConnector->UO->Window);
	}

	static void ResizeWindow()
	{
		RECT R;
		GetClientRect(Globals::scriptConnector->UO->Window, &R);

		Globals::scriptConnector->MainWindow->Height = R.right;
		Globals::scriptConnector->MainWindow->Width = R.bottom;
	}

	static void GetUOWindow()
	{
		HWND hwnd = FindWindowEx(0, 0, L"Ultima Online", 0);
		while(hwnd != 0)
		{
			// Make sure the window is owned by the current thread
			DWORD threadid = GetWindowThreadProcessId(hwnd, NULL);
			if(threadid == GetCurrentThreadId())
			{
				if(UOWindow == 0)
					UOWindow = hwnd;
			}
			// Find the next sibling.
			hwnd = FindWindowEx(0, hwnd, L"Ultima Online", 0);
		}

		if(UOWindow==0)
		{
			HWND hwnd = FindWindowEx(0, 0, L"Ultima Online Third Dawn", 0);
    		while(hwnd != 0)
			{
    			// Make sure the window is owned by the current thread
        		DWORD threadid = GetWindowThreadProcessId(hwnd, NULL);
				if(threadid == GetCurrentThreadId())
    			{
        			if(UOWindow == 0)
						UOWindow = hwnd;
    			}
        		// Find the next sibling.
				hwnd = FindWindowEx(0, hwnd, L"Ultima Online Third Dawn", 0);
    		}
		}

		if(UOWindow==0)
			MessageBox(0, L"Unable to find UO window!\n",0,0);
	}
};

Assembly^ ResolveHandler(Object^ Sender, ResolveEventArgs^ args)
{	
	if(args->Name->Substring(0, args->Name->IndexOf(","))->Contains("resources"))
		return nullptr;

	System::String^ path = Path::Combine(Path::GetDirectoryName(Assembly::GetExecutingAssembly()->Location),  String::Concat(args->Name->Substring(0, args->Name->IndexOf(",")), ".dll"));

	Assembly^ ass = Assembly::LoadFile(path);

	return ass;
}

extern "C"__declspec(dllexport) DWORD __cdecl _GetScriptVersion()
{
	return 0x00150117;
}

extern "C"__declspec(dllexport) void __cdecl _Init(DllInterface *intf)
{
	// System::Diagnostics::Debugger::Launch();

	AppDomain::CurrentDomain->AssemblyResolve += gcnew ResolveEventHandler(ResolveHandler);

	if(intf->Size!=sizeof(DllInterface))
        MessageBox(0,L"Incompatible version of script.dll and injection.dll!\nThe program may crash",L"Error",MB_OK|MB_ICONSTOP);

	try
	{
		Globals::Initialize(intf);
	}
	catch(System::Exception^ ex)
	{
		System::Windows::Forms::MessageBox::Show(ex->Message);
	}	
}

extern "C" __declspec(dllexport) void __cdecl _RunFunction(const char *Name)
{
    Globals::scriptConnector->MainWindow->RunScript(gcnew System::String(Name));
}

extern "C" __declspec(dllexport) void __cdecl _Cleanup()
{    
    /*delete Globals::scriptConnector->MainWindow.Dispose();*/
}

