using namespace InjSharp;

#include "extdll.h"
#include "Stdafx.h"
#include "mycsubs.h"
#include "myvar.h"
#include <stdlib.h>
#include <stdio.h>
#include <vector>

public ref class ScriptConnector : System::MarshalByRefObject
{
public:
	
	MainForm^ MainWindow;
	::DllInterface *UO;
	TList<CFuncTable> *Functions;

	System::Object^ CallFunction(System::String^ funcName, array<System::Object^>^ parametros, System::Int32 paramCount)
	{
		try 
		{
			for(int i = 0; i < Functions->Count; i++)
			{
				if(System::String(Functions->Items[i]->Name).ToLower()->Equals(funcName->ToLower()))
				{				
					TVariable result;
					TList<TVariable> tParams;
					
					for each(System::Object^ p in parametros)
					{
						TVariable t;
						tParams.Add(t);

						if(p->GetType() == System::Double::typeid)
						{
							tParams.Items[tParams.Count - 1]->Type = TVariable::T_Number;
							tParams.Items[tParams.Count - 1]->Data.AsNumber = (double)(System::Double^)p;
						}
						else
						{
							tParams.Items[tParams.Count - 1]->Type = TVariable::T_String;
							tParams.Items[tParams.Count - 1]->Data.AsString = (char*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi((System::String^)p).ToPointer();
						}
					}

					TVariable t;
					tParams.Add(t);
					tParams.Items[tParams.Count - 1]->Type = TVariable::T_Class;
					tParams.Items[tParams.Count - 1]->Data.AsString = TString("INTERNALUOCLASS");

					void* parser = System::Runtime::InteropServices::GCHandle::ToIntPtr(System::Runtime::InteropServices::GCHandle::Alloc(this)).ToPointer();	
					const char *r = Functions->Items[i]->Function(GetLibraryFunctions(), (ParserVariable*)&result, (ParserVariable**)tParams.Items, tParams.Count, (ParserObject*)parser);						

					//System::Diagnostics::Debugger::Launch();
					
					if(result.Type == TVariable::T_String)
						return gcnew System::String(result.Data.AsString.c_str());
					else
					{
						if(result.Type == TVariable::T_Number)
							return (System::Double)result.Data.AsNumber;

						return 0;
					}

					break;
				}
			}
		}
		catch(System::Exception^ e)
		{
			UO->ClientPrint("Ocorreu um Erro no Parser!");
		}

		//UO->ClientPrint("Called by the delegate");

		return 0;
	}

	void CallPrint(System::String^ msg)
	{
		UO->ClientPrint((char*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi((System::String^)msg).ToPointer());
	}

	void ReloadFunctions()
	{
		delete Functions;
		Functions = new TList<CFuncTable>();
		void* parser = System::Runtime::InteropServices::GCHandle::ToIntPtr(System::Runtime::InteropServices::GCHandle::Alloc(this)).ToPointer();	

		UO->AddClasses((ParserObject*)parser, GetLibraryFunctions());
	}

	System::Collections::Generic::List<System::String^>^ GetFunctions()
	{
		System::Collections::Generic::List<System::String^>^ list = gcnew System::Collections::Generic::List<System::String^>();

		for(int i = 0; i < Functions->Count; i++)
		{
			list->Add(gcnew System::String(Functions->Items[i]->Name));
		}

		return list;
	}

	void DoCommand(System::String^ func)
	{
		UO->DoCommand((char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(func));
	}

	virtual System::Object^ InitializeLifetimeService()
		override
	{
		return nullptr;
	}
};