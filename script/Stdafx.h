// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently,
// but are changed infrequently


#using <System.dll>
#include <malloc.h>
#pragma once

template <class T> class TList
{
    int Size;
public:
    T **Items;
    int Count;
    TList()
    {
      Count=0;
      Size=128; Items=(T**)malloc(Size*sizeof(T*));
    }
    TList(const TList &t)
    {
      Count=0;
      Size=t.Size; Items=(T**)malloc(Size*sizeof(T*));
      for(int i=0; i<t.Count; i++)
        Add(t[i]);
    }
    TList &operator=(const TList &t)
    {
      Clear();
      free(Items);
      Count=0;
      Size=t.Size; Items=(T**)malloc(Size*sizeof(T*));
      for(int i=0; i<t.Count; i++)
        Add(t[i]);
      return *this;  
    }
    void Add(const T &w)
    {
      Items[Count]=new T(w);
      Count++;
      if(Count==Size)
      {
          Size+=128;
          Items=(T**)realloc(Items,Size*(sizeof(T*)));
      }
    }
    void Delete(int Who)
    {
      if(Who>=Count||Who<0)
          return;
      delete Items[Who];
      memmove(&Items[Who],&Items[Who+1],(Count-Who-1)*sizeof(T*));

      Count--;
      if(Size-Count>128)
      {
        Size=Count+32;
        Items=(T**)realloc(Items,Size*(sizeof(T*)));
      }
    }
    int Find(const T & Who)
    {
        for (int i=0; i<Count; i++)
            if((*Items[i])==Who)
                return i;
        return -1;
    }
    int IndexOf(const T & Who) {return Find(Who);}
    int FindFrom(int Pos, const T & Who)
    {
        for (int i=Pos; i<Count; i++)
            if((*Items[i])==Who)
                return i;
        return -1;
    }
    void Clear()
    {
        for (int i=0; i<Count; i++)
            delete Items[i];
        Count=0;
    }
    ~TList()
    {
      Clear();
      free(Items);
    }
    bool IsEmpty() {return !Count;}
    T& operator[](int V) const
    {
        return *Items[V];
    }
    T& Last()
    {
        if(IsEmpty())
            throw "TList - taking last value from empty list!!!";
        return (*this)[Count-1];
    }
};