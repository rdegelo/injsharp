﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace CustomLibrary
{
    public class Cooking : InjSharp.InjBundle
    {
        private const string FishType = "0x097A";
        private const string FinishMessage = "Looping aborted|You finished looping";

        Utils Utils;

        public Cooking(InjSharp.InjConnector connector, bool debug)
            : base(connector, debug)
        {
            Utils = new Utils(UO);
        }

        public override void Start()
        {
            if (Debug)
            {
                System.Diagnostics.Debugger.Launch();
            }

            UO.CancelMenu();
            UO.AutoMenu("What do you want to make?", "cooked fishsteak (8)");

            while (true)
            {
                var peixe = Utils.FindType(FishType);

                if (peixe.Count() > 0)
                {
                    Utils.DelJournal(FinishMessage);
                    
                    peixe.First().Use();

                    while (true)
                    {
                        if (UO.InJournal(FinishMessage) > 0)
                            break;

                        Thread.Sleep(500);
                    }
                } 
            }
        }
    }
}
