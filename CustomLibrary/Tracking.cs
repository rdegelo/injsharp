﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CustomLibrary
{
    class Tracking : InjSharp.InjBundle
    {
        Utils Utils;

        public Tracking(InjSharp.InjConnector connector, bool debug)
            : base(connector, debug)
        {
            Utils = new Utils(UO);
        }

        public override void Start()
        {
            if (Debug)
            {
                System.Diagnostics.Debugger.Launch();
            }

            while (true)
            {
                Utils.DelJournal("You fail");
                UO.UseSkill(InjSharp.InjectionSkills.Tracking);

                if (!UO.WarMode())
                    UO.WarMode(true);

                //UO.Attack("0x004A3889"); //Boh
                UO.Attack("0x004A2DBA"); //Batman
                    
                Thread.Sleep(10000);

                if (UO.InJournal("You fail") == 0)
                    UO.RClick(50, 50);

                Thread.Sleep(300);
            }
        }
    }
}
