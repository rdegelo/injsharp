﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace CustomLibrary
{
    public class AnimalTaming : InjSharp.InjBundle
    {
        Utils Utils;
        string Animal
        {
            get
            {
                var objs = Utils.FindType("0x00DB", "-1", "ground");

                foreach (var obj in objs)
                    if (obj.Distance < 4)
                        return obj.Serial;

                return null;
            }
        }

        private const string Fail = "You failed to tame";
        private const string Angered = "angered the beast";
        private const string Unresponsive = "unresponsive to taming";
        private const string Success = "You successfully tame";
        private const string YouMustWait = "You must wait";
        private const string Release = "All Release";
        private const string Saving = "Saving World State";
        private const string Saved = "World Saved";
        
        DateTime? lastSkill;

        public AnimalTaming(InjSharp.InjConnector connector, bool debug)
            : base(connector, debug)
        {
            Utils = new Utils(UO);
        }

        public override void Start()
        {
            if (Debug)
            {
                System.Diagnostics.Debugger.Launch();
            }

            while (true)
            {
                //UnMount();

                Tame();

                Utils.DelJournal(Release);

                UO.WaitTargetObject("0x004B92C9");
                UO.ServerPrint("All Transfer");

                while (true)
                {
                    Thread.Sleep(100);

                    if (UO.InJournal(Release) > 0)
                        break;
                }

                //Mount();                
            }
        }

        public void Tame()
        {
            Utils.DelJournal(Saving);
            Utils.DelJournal(Saved);

            if (lastSkill != null)
                while (DateTime.Now.Subtract(lastSkill.Value).TotalSeconds < 10)
                    Thread.Sleep(100);

            CheckSave();

            lastSkill = DateTime.Now;

            Utils.DelJournal(Fail);
            UO.DeleteJournal(Angered);
            UO.DeleteJournal(Success);
            UO.DeleteJournal(YouMustWait);

            UO.UseSkill(InjSharp.InjectionSkills.Animal_Taming, Animal);

            while (true)
            {
                Thread.Sleep(1000);

                if (UO.InJournal(Success) > 0)
                    break;

                if (UO.InJournal(Angered) > 0 || UO.InJournal(Fail) > 0 || UO.InJournal(YouMustWait) > 0)
                {
                    if (UO.InJournal(Angered) > 0 || UO.InJournal(Unresponsive) > 0)
                        for(int i = 0; i < 5; i++)
                            Lore();

                    Tame();
                    break;
                }
            }
        }

        public void Lore()
        {
            Utils.DelJournal(Saving);
            Utils.DelJournal(Saved);

            if (lastSkill != null)
                while (DateTime.Now.Subtract(lastSkill.Value).TotalSeconds < 10)
                    Thread.Sleep(100);

            CheckSave();

            lastSkill = DateTime.Now;

            UO.UseSkill(InjSharp.InjectionSkills.Animal_Lore, Animal);
        }

        public void UnMount()
        {
            if (IsMounted())
                UO.UseObject("self");

            while (IsMounted())
                Thread.Sleep(100);

            UO.ServerPrint("All Release");
        }

        public void Mount()
        {
            if (!IsMounted())
                UO.UseObject(Animal);

            while (!IsMounted())
                Thread.Sleep(100);
        }

        private bool IsMounted()
        {
            return !string.IsNullOrEmpty(UO.ObjAtLayer(InjSharp.InjectionLayer.Horse)) && UO.ObjAtLayer(InjSharp.InjectionLayer.Horse) != "0";
        }

        private void CheckSave()
        {
            if (UO.InJournal(Saving) > 0)
            {
                while (true)
                {
                    Thread.Sleep(100);

                    if (UO.InJournal(Saved) > 0)
                        break;
                }
            }
        }
    }
}
