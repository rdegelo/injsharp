﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CustomLibrary
{
    public class HelloWorld : InjSharp.InjBundle
    {
        
        public HelloWorld(InjSharp.InjConnector connector, bool debug)
            : base(connector, debug)
        { }

        public override void Start()
        {
            if (Debug)
            {
                System.Diagnostics.Debugger.Launch();
            }

            UO.LClick(59, 86);
        }
    }
}
