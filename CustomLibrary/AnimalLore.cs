﻿using System.Threading;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomLibrary
{
    public class AnimalLore : InjSharp.InjBundle
    {
        Utils Utils;

        public AnimalLore(InjSharp.InjConnector connector, bool debug)
            : base(connector, debug)
        {
            Utils = new Utils(UO);
        }

        public override void Start()
        {
            if (Debug)
            {
                System.Diagnostics.Debugger.Launch();
            }


            var animal = Utils.RequestSerial("Animal");
            var cooldown = Utils.GetCooldown(InjSharp.InjectionSkills.Animal_Lore);

            Thread.Sleep(cooldown);

            while (true)
            {
                UO.UseSkill(InjSharp.InjectionSkills.Animal_Lore, animal);

                Thread.Sleep(cooldown);
            }
        }
    }
}
