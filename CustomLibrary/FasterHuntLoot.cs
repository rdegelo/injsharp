﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CustomLibrary
{
    public class FasterHuntLoot : InjSharp.InjBundle
    {
        Utils Utils;
        readonly TimeSpan LootWait;

        string[] types =
            {
                "0x0F8E", //Pagams
                "0x0F7D",
                "0x0F7F",
                "0x0F7C",
                "0x0F83",
                "0x0F81",
                "0x0F79",
                "0x09B0",
                "0x0F89",
                "0x0F8A",
                "0x0F90",
                "0x0F82",
                "0x0F7E",
                "0x0F80",
                "0x0F87",
                "0x0F78",
                "0x0F8B",
                "0x0F91",
                "0x0F8F",
                "0x0F0F",
                "0x0F30",
                "0x0F13",
                "0x0F16",
                "0x0F10",
                "0x0F18",
                "0x0F15",
                "0x0F11",
                "0x0F16",
                "0x0F25",            

                "0x0EED", //Gold

                "0x0F3F", //Arrows
                "0x0DE1" //Kindlings
            };

        public FasterHuntLoot(InjSharp.InjConnector connector, bool debug)
            : base(connector, debug)
        {
            Utils = new Utils(UO);
            LootWait = new TimeSpan(0, 0, 0, 0, 300);
        }

        public override void Start()
        {
            if (Debug)
            {
                System.Diagnostics.Debugger.Launch();

                Connector.DelGlobal("PagamContainer");
                Connector.DelGlobal("MaigcItemContainer");
            }

            if (Connector.GetGlobal("PagamContainer") == null)
                Connector.SetGlobal("PagamContainer", Utils.RequestSerial("PagamContainer"));

            if (Connector.GetGlobal("MaigcItemContainer") == null)
                Connector.SetGlobal("MaigcItemContainer", Utils.RequestSerial("MaigcItemContainer"));

            UO.Set("finddistance", "2");

            LootContainer("ground");
            UO.Print("Fim");
        }

        private void LootContainer(string objContainer)
        {
            var objsOnContainer = Utils.FindType(container: objContainer);

            Utils.AllNames(objsOnContainer);            

            int count = 0;

            do
            {
                Utils.CheckLag();
                count = 0;

                foreach (var obj in objsOnContainer)
                {
                    if (PodeLootiar(obj.Type) || obj.Name.ToUpper().Contains("MAGIC"))
                    {
                        count++;

                        if (obj.Name.ToUpper().Contains("MAGIC"))
                            obj.MoveTo((string)Connector.GetGlobal("MaigcItemContainer"));
                        else
                            obj.MoveTo((string)Connector.GetGlobal("PagamContainer"));

                        Thread.Sleep(LootWait);
                    }
                    else
                    {
                        if (obj.Name.ToUpper().Contains("CORPSE"))
                        {
                            UO.Print("Isto eh um corpo!");
                            obj.Use();
                            Utils.CheckLag();

                            LootContainer(obj.Serial);
                        }
                    }
                }
            } while (count > 0);
        }

        private bool PodeLootiar(string type)
        {
            return types.Contains(type);
        }
    }
}
