﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CustomLibrary
{
    class Camping : InjSharp.InjBundle
    {
        Utils Utils;

        public Camping(InjSharp.InjConnector connector, bool debug)
            : base(connector, debug)
        {
            Utils = new Utils(UO);
        }

        public override void Start()
        {
            if (Debug)
            {
                System.Diagnostics.Debugger.Launch();
            }

            UO.FindType("0x0DE1", "-1", "ground");
            if (UO.FindCount() > 0)
            {
                while (true)
                {
                    //while (UO.Mana != UO.INT)
                    //{
                        UO.UseObject("finditem");

                        Thread.Sleep(1000);
                    //}

                    //Cast();
                }
            }
        }

        private void Cast()
        {
            Utils.DelJournal("enough mana");

            while (UO.InJournal("enough mana") == 0)
            {
                UO.Cast(InjSharp.InjectionSpell.Magic_Reflection);
                Thread.Sleep(1000);
            }
        }
    }
}
