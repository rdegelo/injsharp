﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace CustomLibrary
{
    public class AnimalTamingClient : InjSharp.InjBundle
    {
        Utils Utils;
        string Animal
        {
            get
            {
                var objs = Utils.FindType("0x00DB", "-1", "ground");

                foreach (var obj in objs)
                    if (obj.Distance < 4)
                        return obj.Serial;

                return null;
            }
        }

        private const string Transfer = "All Transfer";
        private const string Accepts = "accepts Curador as its new master";
        
        public AnimalTamingClient(InjSharp.InjConnector connector, bool debug)
            : base(connector, debug)
        {
            Utils = new Utils(UO);
        }

        public override void Start()
        {
            if (Debug)
            {
                System.Diagnostics.Debugger.Launch();
            }

            while (true)
            {
                //UO.Exec("ongump superrepeat");

                Utils.DelJournal(Accepts);

                while (true)
                {
                    UO.LClick(59, 86);

                    Thread.Sleep(1000);

                    if (UO.InJournal(Accepts) > 0)
                        break;
                }

                Mount();

                UnMount();

                UO.ServerPrint("All Release");
            }
        }

        public void UnMount()
        {
            if (IsMounted())
                UO.UseObject("self");

            while (IsMounted())
                Thread.Sleep(100);

            UO.ServerPrint("All Release");
        }

        public void Mount()
        {
            if (!IsMounted())
                UO.UseObject(Animal);

            while (!IsMounted())
                Thread.Sleep(100);
        }

        private bool IsMounted()
        {
            return !string.IsNullOrEmpty(UO.ObjAtLayer(InjSharp.InjectionLayer.Horse)) && UO.ObjAtLayer(InjSharp.InjectionLayer.Horse) != "0";
        }
    }
}
