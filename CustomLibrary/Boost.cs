﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CustomLibrary
{
    class Boost : InjSharp.InjBundle
    {
        Utils Utils;

        public Boost(InjSharp.InjConnector connector, bool debug)
            : base(connector, debug)
        {
            Utils = new Utils(UO);
        }

        public override void Start()
        {
            if (Debug)
            {
                System.Diagnostics.Debugger.Launch();
            }

            var target = Utils.RequestSerial("Selected Target!");

            UO.Cast(InjSharp.InjectionSpell.Reactive_Armor, target); 
            WaitCast();
            UO.Cast(InjSharp.InjectionSpell.Agility, target); 
            WaitCast();
            UO.Cast(InjSharp.InjectionSpell.Cunning, target); 
            WaitCast();
            UO.Cast(InjSharp.InjectionSpell.Protection, target); 
            WaitCast();
            UO.Cast(InjSharp.InjectionSpell.Strength, target); 
            WaitCast();
            UO.Cast(InjSharp.InjectionSpell.Bless, target); 
            WaitCast();
            UO.Cast(InjSharp.InjectionSpell.Greater_Heal, target);
        }

        private void WaitCast()
        {
            while (UO.Waiting())
                Thread.Sleep(10);
        }
    }
}
