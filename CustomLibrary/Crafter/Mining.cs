﻿using InjSharp.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace CustomLibrary.Crafter
{
    class Mining : InjSharp.InjBundle
    {
        Utils Utils;
        string[] miningToolTypes = { "0x0E85" };

        private const string FailMessage = "You cant see|You can't use|This is too far|That is too far|You must wait";
        private const string FinishMessage = "You can't seem|Looping aborted|You finished looping|There's no ore left";

        public Mining(InjSharp.InjConnector connector, bool debug)
            : base(connector, debug)
        {
            Utils = new Utils(UO);
        }

        public override void Start()
        {
            if (Debug)
            {
                System.Diagnostics.Debugger.Launch();
            }

            //HashSet<Point3D> excludedPoints;
            //Dictionary<Point3D, DateTime> visitedPoints = new Dictionary<Point3D, DateTime>();

            //if (Cache.ContainsKey("MiningExcludedPoints"))
            //    excludedPoints = (HashSet<Point3D>)Cache["MiningExcludedPoints"];
            //else
            //{
            //    excludedPoints = new HashSet<Point3D>();
            //    Cache.Add("MiningExcludedPoints", excludedPoints);
            //}

            UO.Print("Setting up mining spots");
            MiningSpot currentSpot = new MiningSpot() 
            {
                Spot = new Point3D(UO.GetX(), UO.GetY(), UO.GetZ())
            };

            var EUO = GetEUO(UO);

            //Initialize Coords
            int range = 10;
            int z = UO.GetZ();
            HashSet<MiningSpot> miningSpots = new HashSet<MiningSpot>();

            for (int x = UO.GetX() - range; x < UO.GetX() + range; x++)
            {
                for (int y = UO.GetY() - range; y < UO.GetY() + range; y++)
                {
                    var p = new Point3D(x, y, z);

                    miningSpots.Add(new MiningSpot() 
                    {
                        Spot = p
                        ,
                        LastVisited = DateTime.MinValue
                    });
                }
            }

            UO.Print("Starting Digging it deep");

            while (true)
            {
                while (true)
                {
                    var nextSpot = miningSpots.Where(o => (DateTime.Now - o.LastVisited).TotalMinutes > 10).OrderBy(o => Math.Abs(o.Spot.X - currentSpot.Spot.X) + Math.Abs(o.Spot.Y - currentSpot.Spot.Y)).First();

                    EUO.Move(nextSpot.Spot.X, nextSpot.Spot.Y, 0, 5000);

                    Utils.CheckLag();
                    var newPos = new Point3D(UO.GetX(), UO.GetY(), UO.GetZ());
                    if (!newPos.Equals(nextSpot.Spot))
                    {
                        miningSpots.Remove(nextSpot);
                        currentSpot = new MiningSpot() { Spot = newPos };
                    }
                    else
                    {
                        nextSpot.LastVisited = DateTime.Now;
                        currentSpot = nextSpot;
                        break;
                    }
                }

                if (!DoMining(currentSpot.Spot))
                {
                    miningSpots.Remove(currentSpot);
                }
            }
        }

        DateTime lastArmsLore = DateTime.MinValue;

        private bool DoMining(Point3D point)
        {
            var tool = UO.ObjAtLayer(InjSharp.InjectionLayer.Rhand);
            
            if (string.IsNullOrEmpty(tool))
            {
                var tempTool = Utils.FindType(miningToolTypes[0], "-1", "backpack").FirstOrDefault();
                if (tempTool != null)
                    tool = tempTool.Serial;
            }

            if ((DateTime.Now - lastArmsLore).TotalSeconds >= 10)
            {
                lastArmsLore = DateTime.Now;

                UO.CancelTarget();
                if (!string.IsNullOrEmpty(tool))
                    UO.UseSkill(InjSharp.InjectionSkills.Arms_Lore, tool);

                Thread.Sleep(500);
                Utils.CheckLag();
            }

            Utils.DelJournal(FailMessage);
            Utils.DelJournal(FinishMessage);

            UO.WaitTargetTile("0x0E85", point.X, point.Y, point.Z);
            UO.UseObject(tool);

            while (true)
            {
                if (UO.InJournal(FailMessage) > 0)
                    return false;

                if (UO.InJournal(FinishMessage) > 0)
                    return true;

                Thread.Sleep(500);
            }
        }

        private class MiningSpot
        {
            public Point3D Spot { get; set; }
            public DateTime LastVisited { get; set; }
        }
    }
}
