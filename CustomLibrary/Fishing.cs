﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace CustomLibrary
{    
    public class Fishing : InjSharp.InjBundle
    {
        Utils Utils;

        private readonly Dictionary<string, string> ItensToKeep = new Dictionary<string, string>() 
        {
            { "0x14ED", "0x410656C9" } //maps
            ,
            { "0x0DCA", "0x4106934C" } //Fishingnet
            ,
            { "0x099F", "0x410D7CAF" } //SoS
            ,
            { "0x09B2", "0x4107424A" } //Bags
        };

        private const string FishingPoleType = "0x0DBF";
        private const string FishType = "0x097A";
        private const string SeaweedType = "0x0DBA";

        private const string FailMessage = "You cant see|You can't use|This is too far|That is too far|You must wait|You must target water";
        private const string FinishMessage = "You can't seem|Looping aborted|You finished looping";

        private const int Radius = 8;

        static HashSet<Point> excludedPoints = new HashSet<Point>();

        public Fishing(InjSharp.InjConnector connector, bool debug)
            : base(connector, debug)
        {
            Utils = new Utils(UO);
        }

        public override void Start()
        {
            if (Debug)
            {
                System.Diagnostics.Debugger.Launch();
            }

            var pole = Utils.FindType(FishingPoleType).FirstOrDefault();
            if (pole == null)
                return;

            if (Cache.ContainsKey("fishing" + UO.GetX() + "-" + UO.GetY()))
                excludedPoints = (HashSet<Point>)Cache["fishing" + UO.GetX() + "-" + UO.GetY()];

            while (true)
            {
                for (int x = UO.GetX() - Radius; x < UO.GetX() + Radius; x++)
                {
                    DropJunks();

                    for (int y = UO.GetY() - Radius; y < UO.GetY() + Radius; y++)
                    {
                        if (!excludedPoints.Contains(new Point(x, y)))
                        {
                            if (!DoFish(x, y, pole.Serial))
                                excludedPoints.Add(new Point(x, y));

                            Thread.Sleep(1500);
                        }

                        if (UO.Weight >= 400)
                            GotoBankAndComeBack();
                    }
                }

                if (Cache.ContainsKey("fishing" + UO.GetX() + "-" + UO.GetY()))
                    Cache.Remove("fishing" + UO.GetX() + "-" + UO.GetY());

                Cache.Add("fishing" + UO.GetX() + "-" + UO.GetY(), excludedPoints);
            }
        }

        public void DropJunks()
        {
            foreach (var weed in Utils.FindType(SeaweedType, "-1", "backpack"))
            {
                weed.DropHere();
                Thread.Sleep(500);
            }
        }

        public void GotoBankAndComeBack()
        {
            try
            {
                UO.SayU(".disarm 1");

                int x = UO.GetX(), y = UO.GetY();

                do
                {
                    UO.Cast(InjSharp.InjectionSpell.Recall, "0x40BD996D");
                    Thread.Sleep(1000);
                    Utils.CheckLag();
                } while (x == UO.GetX() && y == UO.GetY());

                UO.SayU("bank");
                Utils.CheckLag();

                var peixe = Utils.FindType(FishType);

                if (peixe.Length > 0)
                {
                    peixe.First().MoveTo(UO.ObjAtLayer(InjSharp.InjectionLayer.Bank));
                    Utils.CheckLag();
                }

                x = UO.GetX();
                y = UO.GetY();

                do
                {
                    UO.Cast(InjSharp.InjectionSpell.Recall, "0x40CFCA3E");
                    Thread.Sleep(1000);
                    Utils.CheckLag();
                } while (x == UO.GetX() && y == UO.GetY());

                UO.SayU(".arm 1");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public bool DoFish(int x, int y, string pole)
        {
            Utils.DelJournal(FailMessage);
            Utils.DelJournal(FinishMessage);

            UO.WaitTargetTile("6042", x, y, -5);
            UO.UseObject(pole);

            while (true)
            {
                if (UO.InJournal(FailMessage) > 0)
                    return false;

                if (UO.InJournal(FinishMessage) > 0)
                    return true;

                Thread.Sleep(500);
            }
        }
    }
}
