﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace CustomLibrary.PathMaker
{
    public class PathMaker
    {
        public static void BuildPath(string arqName, InjSharp.InjClasses UO)
        {
            using (var frm = new PeekLocationForm(arqName, UO))
            {
                frm.ShowDialog();
            }
        }

        public static void SavePath(string arqName, List<Point3D> coords)
        {
            using (FileStream fileStream = new FileStream(arqName, FileMode.Create))
            using (TextWriter writter = new StreamWriter(fileStream))
                writter.Write(string.Join("\n", coords.Select(o => string.Format("{0},{1},{2}", o.X, o.Y, o.Z))));
        }

        public static List<Point3D> LoadPath(string arqName)
        {
            List<Point3D> coords = new List<Point3D>();

            using (TextReader reader = new StreamReader(arqName))
            {
                var linhas = reader.ReadToEnd().Split('\n');

                foreach (var linha in linhas)
                {
                    var args = linha.Split(',');
                    coords.Add(new Point3D(int.Parse(args[0]), int.Parse(args[1]), int.Parse(args[2])));
                }                
            }

            return coords;
        }

        public static void MoveToPath(string arqName, InjSharp.InjClasses UO, bool reverse)
        {
            var path = LoadPath(arqName);

            if (reverse)
                path.Reverse();

            var EUO = new uoNet.UO();
            EUO.Open();

            foreach (var pt in path)
            {
                UO.Print(string.Format("Prox. Coord: {0},{1},{2}", pt.X, pt.Y, pt.Z));

                EUO.PathFind(pt.X, pt.Y, pt.Z);

                while (!Point3D.GetCurrentCoord().Equals(pt))
                    Thread.Sleep(500);
            }

            EUO.Close();
        }

        [Serializable]
        public class Point3D
        {
            public int X { get; private set; }
            public int Y { get; private set; }
            public int Z { get; private set; }

            public Point3D(int x, int y, int z)
            {
                this.X = x;
                this.Y = y;
                this.Z = z;
            }

            public override int GetHashCode()
            {
                return (X * Y * Z).GetHashCode();
            }

            public override bool Equals(object obj)
            {
                var item = obj as Point3D;

                if (item != null)
                    if (item.X == this.X && item.Y == this.Y && item.Z == this.Z)
                        return true;

                return false;
            }

            public static PathMaker.Point3D GetCurrentCoord()
            {
                var EUO = new uoNet.UO();
                EUO.Open();

                var curCoord = new PathMaker.Point3D(EUO.CharPosX, EUO.CharPosY, EUO.CharPosZ);

                EUO.Close();
                return curCoord;
            }
        }     
    }
}
