﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Windows.Forms;

namespace CustomLibrary.PathMaker
{
    public partial class PeekLocationForm : Form
    {
        private string arqName;
        private InjSharp.InjClasses UO;
        private List<PathMaker.Point3D> coords = new List<PathMaker.Point3D>();

        public PeekLocationForm(string arqName, InjSharp.InjClasses UO)
        {
            InitializeComponent();

            this.arqName = arqName;
            this.UO = UO;

            coords.Add(PathMaker.Point3D.GetCurrentCoord());
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            timerMonitoraPosicao.Enabled = true;
            timerMonitoraPosicao.Start();

            buttonStart.Enabled = false;
            buttonStop.Enabled = true;
        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            timerMonitoraPosicao.Enabled = false;
            timerMonitoraPosicao.Stop();

            buttonStart.Enabled = true;
            buttonStop.Enabled = false;

            coords.Add(PathMaker.Point3D.GetCurrentCoord());
            PathMaker.SavePath(arqName, coords);
        }

        private void timerMonitoraPosicao_Tick(object sender, EventArgs e)
        {
            var curCoord = PathMaker.Point3D.GetCurrentCoord();

            if (coords.Count == 0 || !coords.Last().Equals(curCoord))
            {
                coords.Add(curCoord);

                UO.Print(string.Format("Coordenada Setada: {0},{1},{2}", coords.Last().X, coords.Last().Y, coords.Last().Z));
            }
        }

        private void buttonVoltar_Click(object sender, EventArgs e)
        {
            PathMaker.MoveToPath(arqName, UO, true);
        }        
    }
}
