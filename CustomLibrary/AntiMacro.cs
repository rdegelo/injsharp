﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace CustomLibrary
{
    public class AntiMacro : InjSharp.InjBundle
    {
        Utils Utils;

        public AntiMacro(InjSharp.InjConnector connector, bool debug)
            : base(connector, debug)
        {
            Utils = new Utils(UO);
        }

        public override void Start()
        {
            if (Debug)
            {
                System.Diagnostics.Debugger.Launch();
            }

            var UO = new uoNet.UO();
            UO.Open();

            int qtdMchecks = 0;
            string lastGumpId = null;
            string lastSerial = null;

            try
            {
                while (true)
                {
                    UO.SysMessage("MChecks passados: " + qtdMchecks, 800);

                    var teste = base.UO.LastGump("replyed");

                    if (base.UO.LastGump("text").Contains("min") && (base.UO.LastGump("gumpid") != lastGumpId || base.UO.LastGump("serial") != lastSerial))
                    {
                        qtdMchecks++;

                        lastGumpId = base.UO.LastGump("gumpid");
                        lastSerial = base.UO.LastGump("serial");

                        uoNet.UO.Container lastContainer = null;
                        int i = 0;

                        while (lastContainer == null || lastContainer.Kind != 0)
                        {
                            lastContainer = UO.GetCont(i);
                            UO.ContTop(i);

                            if (UO.ContName.Contains("generic"))
                            {
                                int x = UO.ContSizeX - 42;
                                int y = UO.ContSizeY - 50 + 20;

                                UO.SysMessage("Click X: " + x, 800);
                                UO.SysMessage("Click Y: " + y, 800);

                                UO.Click(x, y, true, true, true, false);
                            }

                            i++;
                        }

                        Thread.Sleep(new TimeSpan(0, 0, 5));

                        try
                        {
                            AntiMacroSolver.SolveModo2(UO);
                        }
                        catch (Exception)
                        {
                            try
                            {
                                AntiMacroSolver.SolveModo1(UO);
                            }
                            catch (Exception)
                            {
                                System.Media.SystemSounds.Beep.Play();
                                MessageBox.Show("Todas as tentativas de solucionar o MacroCheck falharam.");
                            }
                        }

                        base.UO.UseObject(base.UO.GetSerial(InjSharp.InjectionConst.my));
                    }

                    Thread.Sleep(new TimeSpan(0, 1, 0));
                }
            }
            catch (Exception)
            {
                UO.Close();
            }            
        }

        private static Dictionary<char, Dictionary<Point, int>> Normalizar(Dictionary<char, Dictionary<Point, int>> pt)
        {
            Dictionary<char, Dictionary<Point, int>> newPt = new Dictionary<char, Dictionary<Point, int>>();

            foreach (var p in pt)
            {
                int sub = 0;
                int xOrig = p.Value.Keys.ElementAt(0).X;

                while (xOrig >= 10)
                {
                    xOrig -= 10;
                    sub += 10;
                }

                Dictionary<Point, int> newDict = new Dictionary<Point, int>();
                newPt.Add(p.Key, newDict);

                foreach (var t in p.Value)
                {
                    newDict.Add(new Point(t.Key.X - sub, t.Key.Y), t.Value);
                }
            }

            return newPt;
        }
    }

    public class AntiMacroSolver
    {
        public static void SolveModo1(uoNet.UO UO)
        {
            int xInit, yInit, xEnd, yEnd;
            xInit = 141;
            yInit = 63;
            xEnd = 245;
            yEnd = yInit + 11;

            UO.Click(5, 149, true, true, true, false);

            UO.Wait(10);

            var matrizOrig = GetPixels(UO, xInit, yInit, xEnd, yEnd);

            var mapeamentosOrig = BreakAtPix(matrizOrig, 15724502);
            mapeamentosOrig = mapeamentosOrig.Take(mapeamentosOrig.Length - 1).ToArray();

            xInit = 50;
            yInit = 98;
            xEnd = 245;
            yEnd = yInit + 11;            

            
            for (int i = 0; i < 10; i++)
            {
                var matriz = GetPixels(UO, xInit, yInit, xEnd, yEnd);

                var mapeamentos = BreakAtPix(matriz, 15724502);

                bool match = true;

                if (mapeamentosOrig.Count() == mapeamentos.Count())
                {
                    for (int x = 0; x < mapeamentosOrig.Count(); x++)
                    {
                        if (!mapeamentos[x].SequenceEqual(mapeamentosOrig[x]))
                        {
                            match = false;
                            break;
                        }
                    }

                    if (match)
                    {
                        UO.SysMessage("Opcao encontrada: Y = " + (yInit + 7), 800);

                        UO.Click(1, 149, true, true, true, false);

                        UO.Wait(20);
                        UO.Click(30, yInit + 7, true, true, true, false);
                        UO.Wait(20);
                        UO.Click(30, yInit + 5, true, true, true, false);
                        UO.Click(30, yInit + 9, true, true, true, false);

                        return;
                    }
                }

                yInit = yInit + 20;
                yEnd = yInit + 11;
            }

            throw new Exception("Falha ao tentar reconhecer numeros");
        }

        public static void SolveModo2(uoNet.UO UO)
        {
            int xInit, yInit, xEnd, yEnd;
            xInit = 277;
            yInit = 199;
            xEnd = 450;
            yEnd = yInit + 11;

            var matriz = GetPixels(UO, xInit, yInit, xEnd, yEnd);

            var mapeamentos = BreakAtPix(matriz, 3750201);

            using (var stream = new FileStream("mapeamento", FileMode.Open))
            {
                BinaryFormatter formatter = new BinaryFormatter();

                var patterns = (Dictionary<char, Dictionary<Point, int>>)formatter.Deserialize(stream);

                List<char> foundList = new List<char>();

                foreach (var map in mapeamentos)
                {
                    foreach (var pat in patterns)
                    {
                        if (pat.Value.SequenceEqual(map))
                        {
                            foundList.Add(pat.Key);
                            break;
                        }
                    }
                }

                if (foundList.Count != mapeamentos.Count() || foundList.Count == 0)
                    throw new Exception("Falha ao tentar reconhecer numeros");

                UO.SysMessage("Opcao encontrada: " + string.Join("", foundList), 800);
                UO.Click(311, 244, true, true, true, false);

                UO.Wait(20);

                foreach (var chr in foundList)
                    UO.Key(chr.ToString(), false, false, false);

                UO.Wait(20);

                UO.Click(317, 309, true, true, true, false);
            }
        }

        public static void TrainPattern2(uoNet.UO UO, params char[] chars)
        {
            int xInit, yInit, xEnd, yEnd;
            xInit = 277;
            yInit = 199;
            xEnd = 450;
            yEnd = yInit + 11;

            var matriz = GetPixels(UO, xInit, yInit, xEnd, yEnd);

            var mapeamentos = BreakAtPix(matriz, 3750201);
            
            GravarMapeamento("mapeamento", mapeamentos, chars);
        }

        public static void TrainPattern(uoNet.UO UO, params char[] chars)
        {
            int xInit, yInit, xEnd, yEnd;
            xInit = 141;
            yInit = 63;
            xEnd = 245;
            yEnd = yInit + 11;

            var matriz = GetPixels(UO, xInit, yInit, xEnd, yEnd);

            var mapeamentos = BreakAtPix(matriz, 15724502);
            mapeamentos = mapeamentos.Take(mapeamentos.Length - 1).ToArray();

            GravarMapeamento("mapeamento2", mapeamentos, chars);
        }

        public static Point SeekForPattern(uoNet.UO UO, Dictionary<Point, int> matriz)
        {
            SeekForPatternDelegate seek = SeekForPattern;
            var asyncResults = new List<IAsyncResult>();

            var screenMatrix = GetScreenMatrix(UO);

            foreach (var pt in screenMatrix)
            {
                asyncResults.Add(seek.BeginInvoke(UO, pt.Key, screenMatrix, matriz, null, null));
            }

            while (asyncResults.Count > 0)
                foreach (var asyncResult in asyncResults.ToList())
                {
                    if (asyncResult.IsCompleted)
                    {
                        var returnValue = seek.EndInvoke(asyncResult);
                        if (returnValue != Point.Empty)
                            return returnValue;
                        else
                            asyncResults.Remove(asyncResult);
                    }
                }

            return Point.Empty;
        }

        private static Dictionary<Point, int> GetScreenMatrix(uoNet.UO UO)
        {
            Dictionary<Point, int> screenMatrix = new Dictionary<Point, int>();

            for (int x = 0; x < Math.Min(Screen.PrimaryScreen.Bounds.Width, 900); x++)
                for (int y = 0; y < Math.Min(Screen.PrimaryScreen.Bounds.Height, 900); y++)
                {
                    screenMatrix.Add(new Point(x, y), UO.GetPix(x, y));
                }

            return screenMatrix;
        }

        private delegate Point SeekForPatternDelegate(uoNet.UO UO, Point offset, Dictionary<Point, int> screenMatrix, Dictionary<Point, int> pattern);

        public static Point SeekForPattern(uoNet.UO UO, Point offset, Dictionary<Point, int> screenMatrix, Dictionary<Point, int> pattern)
        {
            int pixFound = 0;

            foreach (var pt in pattern)
            {
                Point newPoint = new Point(pt.Key.X, pt.Key.Y);
                newPoint.Offset(offset);

                try
                {
                    if (screenMatrix[newPoint] == pt.Value)
                        pixFound++;
                }
                catch (Exception)
                {
                    return Point.Empty;
                }
            }

            if (((double)pixFound / pattern.Count) < 0.5)
                return Point.Empty;

            return offset;
        }

        private static Dictionary<Point, int> GetPixels(uoNet.UO UO, int xInit, int yInit, int xEnd, int yEnd)
        {
            Dictionary<Point, int> matriz = new Dictionary<Point, int>();

            int xPos, yPos;
            xPos = 0;
            yPos = 0;

            for (int x = xInit; x <= xEnd; x++)
            {
                for (int y = yInit; y <= yEnd; y++)
                {
                    matriz.Add(new Point(xPos, yPos), UO.GetPix(x, y));

                    yPos++;
                }

                yPos = 0;
                xPos++;
            }

            return matriz;

            //BinaryFormatter formatter = new BinaryFormatter();
            //FileStream stream = new FileStream(nomeArqivo, FileMode.OpenOrCreate);

            //formatter.Serialize(stream, matriz);

            //stream.Close();
        }

        private static Dictionary<Point, int>[] BreakAtPix(Dictionary<Point, int> matriz, int pix)
        {
            List<Dictionary<Point, int>> lista = new List<Dictionary<Point, int>>();
            Dictionary<Point, int> currentDict = null;

            int xPos, yPos;
            xPos = 0;
            yPos = 0;

            for (int x = 0; x <= matriz.Max(o => o.Key.X); x++)
            {
                bool foundPix = false;

                for (int y = 0; y <= matriz.Max(o => o.Key.Y); y++)
                {
                    var pt = new Point(x, y);

                    if (matriz[pt] == pix)
                    {
                        foundPix = true;

                        if (currentDict == null)
                        {
                            currentDict = new Dictionary<Point, int>();
                            lista.Add(currentDict);

                            yPos = 0;
                            xPos = 0;
                        }

                        currentDict.Add(new Point(xPos, yPos), pix);
                    }

                    yPos++;
                }

                yPos = 0;
                xPos++;

                if (!foundPix)
                {
                    currentDict = null;                    
                }
            }

            return lista.ToArray();
        }

        private static void GravarMapeamento(string nomeArquivo, Dictionary<Point, int>[] matrizes, params char[] chrs)
        {
            BinaryFormatter formatter = new BinaryFormatter();

            Dictionary<char, Dictionary<Point, int>> mapeamento = null;

            if (File.Exists(nomeArquivo))
            {
                using (var stream = new FileStream(nomeArquivo, FileMode.Open))
                {
                    mapeamento = (Dictionary<char, Dictionary<Point, int>>)formatter.Deserialize(stream);
                }
            }
            else
                mapeamento = new Dictionary<char, Dictionary<Point, int>>();

            if (matrizes.Length != chrs.Length)
                throw new Exception("Quantidade n bate");

            for (int i = 0; i < matrizes.Length; i++)
            {
                if (mapeamento.ContainsKey(chrs[i]))
                    mapeamento.Remove(chrs[i]);

                mapeamento.Add(chrs[i], matrizes[i]);
            }

            using (FileStream stream = new FileStream(nomeArquivo, FileMode.Create))
            {
                formatter.Serialize(stream, mapeamento);

                stream.Close();
            }
        }
    }
}
