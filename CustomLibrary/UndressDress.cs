﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace CustomLibrary
{
    public class RecordSet : InjSharp.InjBundle
    {
        Utils Utils;
        InjSharp.InjectionLayer[] except = new InjSharp.InjectionLayer[] { InjSharp.InjectionLayer.Bank, InjSharp.InjectionLayer.Bpack, InjSharp.InjectionLayer.Horse, InjSharp.InjectionLayer.None, InjSharp.InjectionLayer.Sell, InjSharp.InjectionLayer.NRstk, InjSharp.InjectionLayer.Rstk };

        public RecordSet(InjSharp.InjConnector connector, bool debug)
            : base(connector, debug)
        {
            Utils = new Utils(UO);
        }

        public override void Start()
        {
            if (Debug)
            {
                System.Diagnostics.Debugger.Launch();                
            }

            Dictionary<InjSharp.InjectionLayer, string> dict = new Dictionary<InjSharp.InjectionLayer, string>();

            foreach(var layer in Enum.GetValues(typeof(InjSharp.InjectionLayer)))
            {
                if (!except.Contains((InjSharp.InjectionLayer)layer))
                {
                    var obj = UO.ObjAtLayer((InjSharp.InjectionLayer)layer);
                    if(!string.IsNullOrEmpty(obj))
                        dict.Add((InjSharp.InjectionLayer)layer, obj);
                }
            }


            Connector.SetGlobal("SET", (object)dict);
        }
    }

    public class Dress : InjSharp.InjBundle
    {
        Utils Utils;
        
        public Dress(InjSharp.InjConnector connector, bool debug)
            : base(connector, debug)
        {
            Utils = new Utils(UO);
        }

        public override void Start()
        {
            if (Debug)
            {
                System.Diagnostics.Debugger.Launch();
            }

            Dictionary<InjSharp.InjectionLayer, string> dict = (Dictionary<InjSharp.InjectionLayer, string>)Connector.GetGlobal("SET");

            while(true)
            {
                bool check = true;

                foreach (var item in dict)
                {
                    if (string.IsNullOrEmpty(UO.ObjAtLayer(item.Key)))
                    {
                        UO.Equip(item.Key, item.Value);
                        Thread.Sleep(800);

                        check = false;
                    }
                }

                if (check)
                    break;
            }
        }
    }
}
