﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using InjSharp;

namespace CustomLibrary
{
    class Utils
    {
        InjClasses UO;

        public Utils(InjClasses uo)
        {
            this.UO = uo;
        }

        public GameObject[] FindType(string type = "-1", string color = "-1", string container = "-1")
        {
            List<GameObject> serials = new List<GameObject>();

            UO.IgnoreReset();

            UO.FindType(type, color, container);

            while(UO.FindCount() > 0)
            {                
                serials.Add(new GameObject(UO.GetSerial(InjectionConst.finditem), UO));

                //UO.SayU("_ignore finditem");
                UO.Ignore(InjectionConst.finditem);

                UO.FindType(type, color, container);
            }

            UO.IgnoreReset();

            foreach (var layer in Enum.GetValues(typeof(InjectionLayer)))
            {
                var obj = UO.ObjAtLayer((InjectionLayer)layer);

                if (type == "-1" || UO.GetGraphic(obj) == type)
                    serials.Add(new GameObject(obj, UO));
            }

            return serials.ToArray();
        }

        public void AllNames(GameObject[] objs)
        {
            AllNames(objs.Select(o => o.Serial).ToArray());
        }

        public void AllNames(string[] serials)
        {
            foreach (var serial in serials)
                UO.Click(serial);

            CheckLag();
        }

        public void CheckLag()
        {
            DelJournal("back pack|Back Pack|backpack");

            UO.Click(InjectionConst.backpack);

            do
            {
                Thread.Sleep(100);
            } while (UO.InJournal("back pack|Back Pack|backpack") == 0);
        }

        public string RequestSerial(string PrintName = "Unknown")
        {
            UO.Print("Select Target: " + PrintName);
            UO.Set("quiet", "1");

            UO.AddObject("RequestSerial");
            while (UO.Targeting())
            {
                Thread.Sleep(1);
            }

            UO.Set("quiet", "0");

            return UO.GetSerial("RequestSerial");
        }

        public void DelJournal(string msg)
        {
            while (UO.InJournal(msg) > 0)
                UO.SetJournalLine(UO.InJournal(msg) - 1, "");
        }

        Dictionary<string, TimeSpan> cooldowns = new Dictionary<string, TimeSpan>();

        public TimeSpan GetCooldown(InjectionSkills skill)
        {
            DateTime dtInicio = DateTime.Now;

            UO.WaitTargetSelf();
            UO.UseSkill(skill);
            CheckLag();

            do
            {
                DelJournal("You must wait");
                UO.WaitTargetSelf();
                UO.UseSkill(skill);
                CheckLag();

                Thread.Sleep(50);                
            } while (UO.InJournal("You must wait") > 0);


            UO.CancelTarget();

            return DateTime.Now - dtInicio;
        }
    }

    class GameObject
    {
        private InjClasses UO;

        public virtual string Serial { get; private set; }
        public virtual string Name 
        {
            get
            {
                return UO.GetName(Serial);
            }
        }
        public virtual int Distance 
        { 
            get 
            {
                return UO.GetDistance(Serial);
            } 
        }
        public virtual int Amount
        {
            get
            {
                return UO.GetQuantity(Serial);
            }
        }
        public virtual string Type 
        {
            get 
            {
                return UO.GetGraphic(Serial);
            }
        }
        public virtual GameObject Container
        {
            get
            {
                var container = UO.ContainerOf(Serial);
                if (container.Contains("0xFFFFFF"))
                    return null;
                else
                    return new GameObject(container, UO);
            }
        }

        public GameObject(string serial, InjClasses UO)
        {
            this.Serial = serial;
            this.UO = UO;
        }

        public virtual void Click()
        {
            UO.Click(Serial);
        }

        public virtual void MoveTo(string container, int quantity = -1)
        {
            UO.MoveItem(Serial, quantity, container);
        }

        public virtual void DropHere()
        {
            UO.DropHere(Serial);
        }

        public virtual void Use()
        {
            UO.UseObject(Serial);
        }
    }

    class LastGump
    {
        InjClasses UO;

        public LastGump(InjClasses UO)
        {
            this.UO = UO;
        }

        public string ID 
        {
            get { return UO.LastGump("gumpid"); }
        }

        public bool Replyed
        {
            get { return UO.LastGump("replyed") == "1"; }
        }
    }
}
