﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CustomLibrary
{
    class BugGold : InjSharp.InjBundle
    {
        Utils Utils;

        public BugGold(InjSharp.InjConnector connector, bool debug)
            : base(connector, debug)
        {
            Utils = new Utils(UO);
        }

        public override void Start()
        {
            if (Debug)
            {
                System.Diagnostics.Debugger.Launch();
            }

            //var adaga = Utils.RequestSerial("Selecione uma Adaga!");

            int i = 0;

            while (true)
            {
                //UO.WaitTargetTile("8198", 1, 1, 0);
                //Thread.Sleep(30);
                //UO.UseObject(adaga);
                //Thread.Sleep(800);

                if (i++ == 5)
                {
                    Thread.Sleep(500);
                    Connector.Function("canceltarget", new object[] {}, 0);
                    Connector.Function("useskill", new object[] { "Animal Lore", "0x004A4F5B" }, 2);
                    Thread.Sleep(1000);                    
                    i = 0;
                }

                Thread.Sleep(1000);  
            }
        }
    }
}
