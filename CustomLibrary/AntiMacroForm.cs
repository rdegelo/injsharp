﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CustomLibrary
{
    public partial class AntiMacroForm : Form
    {
        public AntiMacroForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var UO = new uoNet.UO();
            UO.Open();

            AntiMacroSolver.TrainPattern2(UO, textBox1.Text.Split(',').Select(o => o.First()).ToArray());

            UO.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var UO = new uoNet.UO();
            UO.Open();

            AntiMacroSolver.TrainPattern(UO, textBox1.Text.Split(',').Select(o => o.First()).ToArray());

            UO.Close();
        }
    }
}
