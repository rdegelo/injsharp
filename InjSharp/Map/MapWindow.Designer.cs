﻿namespace InjSharp.Map
{
    partial class MapWindow
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button1 = new System.Windows.Forms.Button();
            this.tabControlMaps = new System.Windows.Forms.TabControl();
            this.tabPageFelucca = new System.Windows.Forms.TabPage();
            this.tabPageTrammel = new System.Windows.Forms.TabPage();
            this.tabPageIlshenar = new System.Windows.Forms.TabPage();
            this.tabPageMalas = new System.Windows.Forms.TabPage();
            this.tabPageTokuno = new System.Windows.Forms.TabPage();
            this.tabPageTerMur = new System.Windows.Forms.TabPage();
            this.mapPanel = new System.Windows.Forms.Panel();
            this.uoMapControl1 = new InjSharp.Map.UOMapControl();
            this.panel1 = new System.Windows.Forms.Panel();
            this.checkBoxTrack = new System.Windows.Forms.CheckBox();
            this.trackTimer = new System.Windows.Forms.Timer(this.components);
            this.tabControlMaps.SuspendLayout();
            this.mapPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uoMapControl1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.Location = new System.Drawing.Point(54, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(281, 22);
            this.button1.TabIndex = 1;
            this.button1.Text = "Popup";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tabControlMaps
            // 
            this.tabControlMaps.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControlMaps.Controls.Add(this.tabPageFelucca);
            this.tabControlMaps.Controls.Add(this.tabPageTrammel);
            this.tabControlMaps.Controls.Add(this.tabPageIlshenar);
            this.tabControlMaps.Controls.Add(this.tabPageMalas);
            this.tabControlMaps.Controls.Add(this.tabPageTokuno);
            this.tabControlMaps.Controls.Add(this.tabPageTerMur);
            this.tabControlMaps.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabControlMaps.Location = new System.Drawing.Point(0, 22);
            this.tabControlMaps.Name = "tabControlMaps";
            this.tabControlMaps.SelectedIndex = 0;
            this.tabControlMaps.Size = new System.Drawing.Size(335, 20);
            this.tabControlMaps.TabIndex = 5;
            this.tabControlMaps.SelectedIndexChanged += new System.EventHandler(this.tabControlMaps_SelectedIndexChanged);
            // 
            // tabPageFelucca
            // 
            this.tabPageFelucca.Location = new System.Drawing.Point(4, 25);
            this.tabPageFelucca.Margin = new System.Windows.Forms.Padding(0);
            this.tabPageFelucca.Name = "tabPageFelucca";
            this.tabPageFelucca.Size = new System.Drawing.Size(327, 0);
            this.tabPageFelucca.TabIndex = 0;
            this.tabPageFelucca.Text = "Felucca";
            this.tabPageFelucca.UseVisualStyleBackColor = true;
            // 
            // tabPageTrammel
            // 
            this.tabPageTrammel.Location = new System.Drawing.Point(4, 25);
            this.tabPageTrammel.Margin = new System.Windows.Forms.Padding(0);
            this.tabPageTrammel.Name = "tabPageTrammel";
            this.tabPageTrammel.Size = new System.Drawing.Size(327, 0);
            this.tabPageTrammel.TabIndex = 1;
            this.tabPageTrammel.Text = "Trammel";
            this.tabPageTrammel.UseVisualStyleBackColor = true;
            // 
            // tabPageIlshenar
            // 
            this.tabPageIlshenar.Location = new System.Drawing.Point(4, 25);
            this.tabPageIlshenar.Margin = new System.Windows.Forms.Padding(0);
            this.tabPageIlshenar.Name = "tabPageIlshenar";
            this.tabPageIlshenar.Size = new System.Drawing.Size(327, 0);
            this.tabPageIlshenar.TabIndex = 2;
            this.tabPageIlshenar.Text = "Ilshenar";
            this.tabPageIlshenar.UseVisualStyleBackColor = true;
            // 
            // tabPageMalas
            // 
            this.tabPageMalas.Location = new System.Drawing.Point(4, 25);
            this.tabPageMalas.Margin = new System.Windows.Forms.Padding(0);
            this.tabPageMalas.Name = "tabPageMalas";
            this.tabPageMalas.Size = new System.Drawing.Size(327, 0);
            this.tabPageMalas.TabIndex = 3;
            this.tabPageMalas.Text = "Malas";
            this.tabPageMalas.UseVisualStyleBackColor = true;
            // 
            // tabPageTokuno
            // 
            this.tabPageTokuno.Location = new System.Drawing.Point(4, 25);
            this.tabPageTokuno.Margin = new System.Windows.Forms.Padding(0);
            this.tabPageTokuno.Name = "tabPageTokuno";
            this.tabPageTokuno.Size = new System.Drawing.Size(327, 0);
            this.tabPageTokuno.TabIndex = 4;
            this.tabPageTokuno.Text = "Tokuno";
            this.tabPageTokuno.UseVisualStyleBackColor = true;
            // 
            // tabPageTerMur
            // 
            this.tabPageTerMur.Location = new System.Drawing.Point(4, 25);
            this.tabPageTerMur.Name = "tabPageTerMur";
            this.tabPageTerMur.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageTerMur.Size = new System.Drawing.Size(327, 0);
            this.tabPageTerMur.TabIndex = 5;
            this.tabPageTerMur.Text = "Ter Mur";
            this.tabPageTerMur.UseVisualStyleBackColor = true;
            // 
            // mapPanel
            // 
            this.mapPanel.Controls.Add(this.uoMapControl1);
            this.mapPanel.Controls.Add(this.tabControlMaps);
            this.mapPanel.Controls.Add(this.panel1);
            this.mapPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mapPanel.Location = new System.Drawing.Point(0, 0);
            this.mapPanel.Name = "mapPanel";
            this.mapPanel.Size = new System.Drawing.Size(335, 276);
            this.mapPanel.TabIndex = 6;
            // 
            // uoMapControl1
            // 
            this.uoMapControl1.Active = false;
            this.uoMapControl1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.uoMapControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uoMapControl1.Location = new System.Drawing.Point(0, 42);
            this.uoMapControl1.Name = "uoMapControl1";
            this.uoMapControl1.Size = new System.Drawing.Size(335, 234);
            this.uoMapControl1.TabIndex = 6;
            this.uoMapControl1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.checkBoxTrack);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(335, 22);
            this.panel1.TabIndex = 7;
            // 
            // checkBoxTrack
            // 
            this.checkBoxTrack.AutoSize = true;
            this.checkBoxTrack.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxTrack.Location = new System.Drawing.Point(0, 0);
            this.checkBoxTrack.Name = "checkBoxTrack";
            this.checkBoxTrack.Size = new System.Drawing.Size(54, 22);
            this.checkBoxTrack.TabIndex = 6;
            this.checkBoxTrack.Text = "Track";
            this.checkBoxTrack.UseVisualStyleBackColor = true;
            this.checkBoxTrack.CheckedChanged += new System.EventHandler(this.checkBoxTrack_CheckedChanged);
            // 
            // trackTimer
            // 
            this.trackTimer.Tick += new System.EventHandler(this.trackTimer_Tick);
            // 
            // MapWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.mapPanel);
            this.Name = "MapWindow";
            this.Size = new System.Drawing.Size(335, 276);
            this.tabControlMaps.ResumeLayout(false);
            this.mapPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uoMapControl1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TabControl tabControlMaps;
        private System.Windows.Forms.TabPage tabPageFelucca;
        private System.Windows.Forms.TabPage tabPageTrammel;
        private System.Windows.Forms.TabPage tabPageIlshenar;
        private System.Windows.Forms.TabPage tabPageMalas;
        private System.Windows.Forms.TabPage tabPageTokuno;
        private System.Windows.Forms.TabPage tabPageTerMur;
        private System.Windows.Forms.Panel mapPanel;
        private System.Windows.Forms.CheckBox checkBoxTrack;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Timer trackTimer;
        private UOMapControl uoMapControl1;
    }
}
