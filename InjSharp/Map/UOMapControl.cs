using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Collections;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using InjSharp.Core;

namespace InjSharp.Map
{
	public class UOMapControl : PictureBox
	{
        private bool m_Active;
		
        private Point3D CurrentPosition = Point3D.Zero;
        private int CurrentMap = 0;
	    
        private const double RotateAngle = Math.PI / 4 + Math.PI;
        private DateTime LastRefresh;
        private float m_Zoom = 1.5f;

		public UOMapControl()
		{
			Active = false;
			
			this.BorderStyle = BorderStyle.Fixed3D;
			
            ParentChanged += UOMapControl_ParentChanged;
		}

        #region pan & zoom
        bool panIsActive = false;
        DateTime lastUpdate = DateTime.Now;
        Point3D panStartingPoint = Point3D.Zero;

        void UOMapControl_ParentChanged(object sender, EventArgs e)
        {
            if(FindForm() != null)
            {
                FindForm().MouseWheel += UOMapControl_MouseWheel;
            }
        }

        void UOMapControl_MouseWheel(object sender, MouseEventArgs e)
        {
            if (this.Bounds.Contains(this.Parent.PointToClient(Cursor.Position)))
            {
                if (e.Delta < 0)
                {
                    m_Zoom -= 0.1f;
                }
                else if (e.Delta > 0)
                {
                    m_Zoom += 0.1f;
                }

                CheckZoomBounds();
                UpdateMapServerLocation();
            }
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            panIsActive = true;
            this.DoubleBuffered = true;
            lastUpdate = DateTime.Now;
            panStartingPoint = new Point3D(e.Location.X, e.Location.Y, 0);

            base.OnMouseDown(e);
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            panIsActive = false;
            UpdateMapServerLocation();

            base.OnMouseLeave(e);
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (panIsActive)
            {
                var offset = panStartingPoint - new Point3D(e.Location.X, e.Location.Y, 0);
                offset.X = (int)(offset.X * 1.7);
                offset.Y = (int)(offset.Y * 1.7);

                var a = -45 * System.Math.PI / 180.0;
                float cosa = (float)Math.Cos(a);
                float sina = (float)Math.Sin(a);
                PointF newPoint = new PointF(((float)offset.X * cosa - (float)offset.Y * sina), ((float)offset.X * sina + (float)offset.Y * cosa));

                CurrentPosition.X += (int)newPoint.X;
                CurrentPosition.Y += (int)newPoint.Y;

                CheckPositionBounds();

                if ((DateTime.Now - lastUpdate).TotalSeconds >= 0.1)
                {
                    UpdateMapServerLocation();
                    lastUpdate = DateTime.Now;
                }

                panStartingPoint = new Point3D(e.Location.X, e.Location.Y, 0);
            }

            base.OnMouseMove(e);
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            panIsActive = false;
            UpdateMapServerLocation();

            base.OnMouseUp(e);
        }

        private void CheckPositionBounds()
        {
            var map = Core.Map.GetMap(this.CurrentMap);

            CurrentPosition.X = Math.Max(CurrentPosition.X, 0);
            CurrentPosition.Y = Math.Max(CurrentPosition.Y, 0);

            CurrentPosition.X = Math.Min(CurrentPosition.X, map.Width);
            CurrentPosition.Y = Math.Min(CurrentPosition.Y, map.Height);
        }

        private void CheckZoomBounds()
        {
            if (m_Zoom > 3)
                m_Zoom = 3;

            if (m_Zoom < 0)
                m_Zoom = 0;
        }
        #endregion

        public override void Refresh()
        {
            TimeSpan now =  DateTime.Now - LastRefresh;
            if (now.TotalMilliseconds <= 100)
                return;
            LastRefresh = DateTime.Now;

            UpdateMapServerLocation();
            base.Refresh();
        }

        //private PointF AdjustPoint(PointF center, PointF pos)
        //{
        //    PointF newp = new PointF(center.X - pos.X, center.Y - pos.Y);
        //    float dis = (float)Distance(center, pos);
        //    dis += dis * 0.50f;
        //    float slope = 0;
        //    if (newp.X != 0)
        //        slope = (float)newp.Y / (float)newp.X;
        //    else
        //        return new PointF(0 + center.X, -1f * (newp.Y + (newp.Y * 0.25f)) + center.Y);
        //    slope *= -1;
        //    //Both of these algorithms oddly produce the same results.
        //    //float x = dis / (float)(Math.Sqrt(1f + Math.Pow(slope, 2)));
        //    float x = newp.X + (newp.X * 0.5f);
        //    // if (newp.X > 0)
        //    x *= -1;
        //    float y = (-1) * slope * x;

        //    PointF def = new PointF(x + center.X, y + center.Y);

        //    return def;
        //}

        //public double Distance(PointF center, PointF pos)
        //{

        //    PointF newp = new PointF(center.X - pos.X, center.Y - pos.Y);
        //    double distX = Math.Pow(newp.X,2);
        //    double distY = Math.Pow(newp.Y, 2);
        //    return Math.Sqrt(distX + distY);
        //}
        //private PointF RotatePoint(PointF center, PointF pos)
        //{
        //    PointF newp = new PointF(center.X - pos.X, center.Y - pos.Y);
        //    double x = newp.X * Math.Cos(RotateAngle ) - newp.Y * Math.Sin(RotateAngle);
        //    double y = newp.X * Math.Sin(RotateAngle ) + newp.Y * Math.Sin(RotateAngle);
        //    return AdjustPoint(center, new PointF((float)(x) + center.X, (float)(y) + center.Y));
        //}
		
		protected override void OnPaint(PaintEventArgs pe)
		{			
			base.OnPaint(pe);
		}

        //private Point MousePointToMapPoint(Point p)
        //{
        //    double rad = (Math.PI / 180) * 45;
        //    int w = (this.Width) >> 3;
        //    int h = (this.Height) >> 3;
        //    Point3D focus = CurrentPosition;
		
        //    Point mapOrigin = new Point((focus.X >> 3) - (w / 2), (focus.Y >> 3) - (h / 2));
        //    Point pnt1 = new Point((mapOrigin.X << 3) + (p.X), (mapOrigin.Y << 3) + (p.Y));
        //    Point check = new Point(pnt1.X - focus.X, pnt1.Y - focus.Y);
        //    check = RotatePoint(new Point((int)(check.X * 0.695), (int)(check.Y * 0.68)), rad, 1);
        //    return new Point(check.X + focus.X, check.Y + focus.Y);
        //}

        //private Point RotatePoint(Point p, double angle, double dist)
        //{
        //    int x = (int)((p.X * Math.Cos(angle) + p.Y * Math.Sin(angle)) * dist);
        //    int y = (int)((-p.X * Math.Sin(angle) + p.Y * Math.Cos(angle)) * dist);

        //    return new Point(x, y);
        //}

        //private UOMapRuneButton ButtonCheck(Rectangle rec)
        //{
        //    ArrayList buttons = new ArrayList();
        //    foreach (UOMapRuneButton mbutton in this.m_MapButtons)
        //    {
        //        Rectangle rec2 = new Rectangle( mbutton.X, mbutton.Y, 10, 10 );
        //        if (rec2.IntersectsWith(rec))
        //            return mbutton;

        //    }
        //    return null;
        //}

		protected override void Dispose(bool disposing)
		{
            if(mapServer != null)
                mapServer.Stop();

			base.Dispose(disposing);
		}

		protected override void OnResize(EventArgs e)
		{
			base.OnResize(e);
            UpdateMapServerLocation();
		}

        public void UpdateLocation(Point3D position, int map)
        {
            if (CurrentPosition != position || CurrentMap != map)
            {
                CurrentPosition = position;
                CurrentMap = map;

                UpdateMapServerLocation();
            }
        }

        MapPipeServer mapServer = null;

        private void UpdateMapServerLocation()
        {
            if (mapServer != null)
            {
                var cmd = string.Format("{0} {1} {2} {3} {4}", CurrentPosition.X, CurrentPosition.Y, CurrentPosition.Z, CurrentMap, m_Zoom);
                mapServer.Send(cmd);
            }
        }

		public bool Active
		{
			get { return m_Active; }
			set 
            { 
                m_Active = value;
                if (value)
                {
                    if (mapServer == null)
                    {
                        mapServer = new MapPipeServer();
                        mapServer.Start(() => UpdateMapServerLocation(), this.Handle.ToInt32());
                    }
                    else
                        UpdateMapServerLocation();
                }
            }
		}
	}
}
