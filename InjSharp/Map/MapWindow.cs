﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace InjSharp.Map
{
    public partial class MapWindow : UserControl
    {
        Form outWindow;

        public MapWindow()
        {
            InitializeComponent();

            if (LicenseManager.UsageMode != LicenseUsageMode.Designtime)
            {
                uoMapControl1.Active = true;
                uoMapControl1.UpdateLocation(new Core.Point3D(1323, 1624, 0), 0);
            }
        }

        private void CreateOuterForm()
        {
            outWindow = new Form();
            outWindow.FormBorderStyle = FormBorderStyle.FixedToolWindow;
            outWindow.ShowInTaskbar = false;
            outWindow.Width = 800;
            outWindow.Height = 600;
            outWindow.MaximizeBox = false;
            outWindow.FormClosed += outWindow_FormClosed;
        }

        void outWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            outWindow.Controls.Remove(mapPanel);
            Controls.Add(mapPanel);
            Controls.SetChildIndex(mapPanel, 0);
            button1.Enabled = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CreateOuterForm();

            Controls.Remove(mapPanel);
            outWindow.Controls.Add(mapPanel);
            button1.Enabled = false;

            outWindow.Show();
        }

        private void tabControlMaps_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!checkBoxTrack.Checked)
            {
                var map = Core.Map.GetMap(tabControlMaps.SelectedIndex);

                uoMapControl1.UpdateLocation(new Core.Point3D((int)(map.Width / 2), (int)(map.Height / 2), 0), tabControlMaps.SelectedIndex);
            }
        }

        private void checkBoxTrack_CheckedChanged(object sender, EventArgs e)
        {
            trackTimer.Enabled = checkBoxTrack.Checked;
        }

        private void trackTimer_Tick(object sender, EventArgs e)
        {
            int x = 0, y = 0, z = 0, m = 0;

            var charName = new InjClasses(InjConnector.Instance).GetName(InjectionConst.self);
            if (!string.IsNullOrEmpty(charName))
            {
                Ultima.Client.FindLocation(ref x, ref y, ref z, ref m);
                tabControlMaps.SelectedIndex = m;

                uoMapControl1.UpdateLocation(new Core.Point3D(x, y, z), m);
            }
        }
    }
}
