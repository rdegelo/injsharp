﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading;

namespace InjSharp.Map
{
    public class MapPipeServer
    {
        private NamedPipeServerStream m_PipeServerStream;
        private bool m_IsDisposing;
        private bool m_IsConnected = false;
        private Thread m_PipeMessagingThread;
        private Process m_ChildProcess;
        private string m_pipeName;
        public const int BUFFER_SIZE = 1024 * 2;

        public delegate void ClientConnected();
        private static ClientConnected Callback;

        public bool Start(ClientConnected callback, int handle)
        {
            Callback = callback;

            m_pipeName = "InjSharpMapPipe" + DateTime.Now.ToString("yyyyMMddhhmmss");

            m_PipeMessagingThread = new Thread(new ThreadStart(Init));
            m_PipeMessagingThread.Name = "MapPipeMessagingThread";
            m_PipeMessagingThread.IsBackground = true;
            m_PipeMessagingThread.Start();

            ProcessStartInfo processInfo = new ProcessStartInfo("InjSharpMapHandler", m_pipeName + " " + handle);
            m_ChildProcess = Process.Start(processInfo);

            return true;
        }

        public bool Send(string paramData)
        {
            if (m_IsConnected)
            {
                StreamWriter writer = new StreamWriter(m_PipeServerStream);
                writer.WriteLine(paramData);
                writer.Flush();
            }

            return true;
        }

        private void Init()
        {
            if (m_PipeServerStream == null)
            {
                m_PipeServerStream = new NamedPipeServerStream(m_pipeName,
                                                              PipeDirection.InOut,
                                                              1,
                                                              PipeTransmissionMode.Byte,
                                                              PipeOptions.Asynchronous,
                                                              BUFFER_SIZE,
                                                              BUFFER_SIZE);

                try
                {
                    //Wait for connection from the child process
                    m_PipeServerStream.WaitForConnection();
                    Console.WriteLine(string.Format("Child process is connected."));
                    m_IsConnected = true;

                    Callback();
                }
                catch (ObjectDisposedException exDisposed)
                {
                    Console.WriteLine(string.Format("StartIPCServer for process error: {0}", exDisposed.Message));
                }
                catch (IOException exIO)
                {
                    Console.WriteLine(string.Format("StartIPCServer for process error: {0}", exIO.Message));
                }

                //Start listening for incoming messages.
                bool retRead = true; ;
                while (retRead && !m_IsDisposing)
                {
                    retRead = StartAsyncReceive();
                    Thread.Sleep(30);
                }
            }

        }

        public void Stop()
        {
            DisposeClientProcess();
        }

        bool StartAsyncReceive()
        {
            StreamReader sr = new StreamReader(m_PipeServerStream);
            try
            {
                string str = sr.ReadLine();

                if (string.IsNullOrEmpty(str))
                {
                    // The client is down
                    return false;
                }

                Console.WriteLine(string.Format("Received: {0}. (Thread {1})", str, Thread.CurrentThread.ManagedThreadId));
            }
            catch (Exception e)
            {
                Console.WriteLine("AsyncReceive ERROR: {0}", e.Message);
                return false;
            }

            return true;
        }
        void DisposeClientProcess()
        {
            try
            {
                m_IsDisposing = true;

                try
                {
                    //I will fails if the process doesn't exist
                    m_ChildProcess.Kill();
                }
                catch
                { }

                m_PipeServerStream.Dispose();//This will stop any pipe activity

                Console.WriteLine(string.Format("Process is Closed"));
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("Process is Close error: {0}", ex.Message));
            }

        }
    }
}
