﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace InjSharp.Script
{
    public interface IScriptParam
    {
        Control GetVisualEditor();
        Object GetValue();
        void SetValue();
    }
}
