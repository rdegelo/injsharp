﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;

namespace InjSharp
{
    public unsafe class InjAttributes : MarshalByRefObject
    {
        public override object InitializeLifetimeService()
        {
            return null;
        }        

        public Int32 *Life { get; set; }
        public Int32* STR { get; set; }
        public Int32* Mana { get; set; }
        public Int32* INT { get; set; }
        public Int32* Stamina { get; set; }
        public Int32* DEX { get; set; }
        public Int32* Armor { get; set; }
        public Int32* Weight { get; set; }
        public Int32* Gold { get; set; }
        public Int32* BM { get; set; }
        public Int32* BP { get; set; }
        public Int32* GA { get; set; }
        public Int32* GS { get; set; }
        public Int32* MR { get; set; }
        public Int32* NS { get; set; }
        public Int32* SA { get; set; }
        public Int32* SS { get; set; }
        public Int32* VA { get; set; }
        public Int32* EN { get; set; }
        public Int32* WH { get; set; }
        public Int32* FD { get; set; }
        public Int32* BR { get; set; }
        public Int32* H { get; set; }
        public Int32* C { get; set; }
        public Int32* M { get; set; }
        public Int32* L { get; set; }
        public Int32* B { get; set; }
        public Int32* AR { get; set; }
        public Int32* BT { get; set; }
    }

    public class InjConnector : MarshalByRefObject
    {
        public static InjConnector Instance { get; set; }

        public InjConnector()
        {
            Globals = new Dictionary<string, object>();
            Attributes = new InjAttributes();
            Instance = this;
        }

        public override object InitializeLifetimeService()
        {
            return null;
        }

        public Func<string, object[], int, object> Function;
        public Action<string> Print;
        public Action ReloadFunctions;
        public Func<List<String>> GetFunctions;
        public Action<String> DoCommand;

        public IntPtr UOWindowHandle { get; set; }
        public IntPtr ParentWindowHandle { get; set; }

        public InjAttributes Attributes;

        private Dictionary<string, object> Globals;

        [System.Runtime.ExceptionServices.HandleProcessCorruptedStateExceptions]
        public object ExecFunction(string funcName, object[] args, int argsCount)
        {
            Thread.Sleep(10);

            //StreamWriter writer = new StreamWriter(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "InjSharpLog.txt"), true);

            //writer.Write(string.Format("{0};{1}", funcName, string.Join("|", args.Select(o => o.ToString()).ToArray())));

            object result = null;

            try
            {
                result =  Function(funcName, args, argsCount);
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Windows.Forms.Form.ActiveForm, ex.Message);
            }

            //writer.Write(";" + result.ToString());
            //writer.WriteLine();
            //writer.Close();

            return result;
        }

        public object GetGlobal(string key)
        {
            lock (Globals)
            {
                if (Globals.ContainsKey(key))
                    return Globals[key];
                else
                    return null;
            }
        }

        public void SetGlobal(string key, object val)
        {
            lock (Globals)
            {
                if (Globals.ContainsKey(key))
                    Globals[key] = val;
                else
                    Globals.Add(key, val);
            }
        }

        public void DelGlobal(string key)
        {
            lock (Globals)
            {
                if (Globals.ContainsKey(key))
                    Globals.Remove(key);
            }
        }
    }
}
