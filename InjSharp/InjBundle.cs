﻿using InjSharp.Core;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace InjSharp
{
    [Serializable]
    public abstract class InjBundle : IDisposable
    {
        protected virtual InjClasses UO { get; private set; }
        protected virtual uoNet.UO EUO { get; private set; }

        protected InjConnector Connector { get; private set; }

        protected bool Debug { get; private set; }

        protected static Dictionary<string, object> Cache = new Dictionary<string, object>();

        public InjBundle(InjConnector connector, bool debug)
        {
            this.Connector = connector;
            this.Debug = debug;
            UO = new InjClasses(Connector);
            EUO = GetEUO(UO);
        }

        public static uoNet.UO GetEUO(InjClasses uo)
        {
            uoNet.UO euo = new uoNet.UO();
            euo.Open();

            int charId = int.Parse(uo.GetSerial("self").Replace("0x", null), System.Globalization.NumberStyles.HexNumber);

            while (euo.CharID != charId)
                euo.CliNr++;

            return euo;
        }

        public abstract void Start();

        public virtual void RegisterParameters()
        {

        }

        public void Dispose()
        {
            try
            {
                EUO.Close();
            }
            catch (Exception)
            {                
            }
        }
    }

    public enum InjectionConst
    {
        backpack
        ,
        my
        ,
        self
        ,
        ground
        ,
        finditem
        ,
        lasttarget
    }

    public enum InjectionSkills
    {
        Alchemy,
        Anatomy,
        Animal_Lore,
        Item_ID,
        Arms_Lore,
        Parrying,
        Begging,
        Blacksmithing,
        Bowcraft,
        Peacemaking,
        Camping,
        Carpentry,
        Cartography,
        Cooking,
        Detect_Hidden,
        Enticement,
        Evaluate_Intelligence,
        Healing,
        Fishing,
        Forensic_Evaluation,
        Herding,
        Hiding,
        Provocation,
        Inscription,
        Lockpicking,
        Magery,
        Magic_Resistance,
        Tactics,
        Snooping,
        Musicianship,
        Poisoning,
        Archery,
        Spirit_Speak,
        Stealing,
        Tailoring,
        Animal_Taming,
        Taste_Identification,
        Tinkering,
        Tracking,
        Veterinary,
        Swordsmanship,
        Mace_Fighting,
        Fencing,
        Wrestling,
        Lumberjacking,
        Mining,
        Meditation,
        Stealth,
        Remove_Trap,
        Necromancy
    }

    public enum InjectionLayer
    {
        None,
        Rhand,
        Lhand,
        Shoes,
        Pants,
        Shirt,
        Hat,
        Gloves,
        Ring,
        Neck,
        Hair,
        Waist,
        Torso,
        Brace,
        Beard,
        TorsoH,
        Ear,
        Arms,
        Cloak,
        Bpack,
        Robe,
        Eggs,
        Legs,
        Horse,
        Rstk,
        NRstk,
        Sell,
        Bank
    }

    public enum InjectionSkillVal
    {
        Value = 0,
        RealValue = 1,
        Increase = 2,
        Status = 3
    }

    public enum InjectionSpell
    {
        Clumsy,
        Create_Food,
        Feeblemind,
        Heal,
        Magic_Arrow,
        Night_Sight,
        Reactive_Armor,
        Weaken,
        Agility,
        Cunning,
        Cure,
        Harm,
        Magic_Trap,
        Magic_Untrap,
        Protection,
        Strength,
        Bless,
        Fireball,
        Magic_Lock,
        Poison,
        Telekinesis,
        Teleport,
        Unlock,
        Wall_of_Stone,
        Arch_Cure,
        Arch_Protection,
        Curse,
        Fire_Field,
        Greater_Heal,
        Lightning,
        Mana_Drain,
        Recall,
        Blade_Spirits,
        Dispel_Field,
        Incognito,
        Magic_Reflection,
        Mind_Blast,
        Paralyze,
        Poison_Field,
        Summ__Creature,
        Dispel,
        Energy_Bolt,
        Explosion,
        Invisibility,
        Mark,
        Mass_Curse,
        Paralyze_Field,
        Reveal,
        Chain_Lightning,
        Energy_Field,
        Flame_Strike,
        Gate_Travel,
        Mana_Vampire,
        Mass_Dispel,
        Meteor_Swarm,
        Polymorph,
        Earthquake,
        Energy_Vortex,
        Resurrection,
        Air_Elemental,
        Summon_Daemon,
        Earth_Elemental,
        Fire_Elemental,
        Water_Elemental
    }

    public class InjClasses : MarshalByRefObject
    {
        #region Inputs
        private const int WM_KEYDOWN = 0x100;
        private const int WM_KEYUP = 0x101;
        private const int WM_CHAR = 0x105;
        private const int WM_SYSKEYDOWN = 0x104;
        private const int WM_SYSKEYUP = 0x105;
        private const int WM_SETCURSOR = 0x0020;
        private const int WM_MOUSEMOVE = 0x0200;
        private const int WM_LBUTTONDOWN = 0x0201;
        private const int WM_LBUTTONUP = 0x0202;
        private const int WM_LBUTTONDBLCLK = 0x0203;
        private const int WM_RBUTTONDOWN = 0x0204;
        private const int WM_RBUTTONUP = 0x0205;
        private const int WM_RBUTTONDBLCLK = 0x0206;
        private const int MK_LBUTTON = 0x0001; 
        private const int MK_RBUTTON = 0x0002;
        private const int HTCLIENT = 0x1;

        [DllImport("user32.dll")]
        static extern int SendMessage(IntPtr hWnd, int msg, int wParam, int lParam);

        protected InjConnector Connector { get; private set; }

        public InjClasses(InjConnector connector)
        {
            this.Connector = connector;            
        }

        public override object InitializeLifetimeService()
        {
            return null;
        }

        public virtual void SendString(string text)
        {
            foreach (var chr in text.ToCharArray())
                SendMessage(Connector.UOWindowHandle, WM_CHAR, Convert.ToInt32(chr), 1);

            SendMessage(Connector.UOWindowHandle, WM_CHAR, 13, 1);
        }

        public virtual void SendKey(params Keys[] keys)
        {
            foreach(var key in keys)
                SendMessage(Connector.UOWindowHandle, WM_KEYDOWN, (int)key, 1);

            foreach (var key in keys)
                SendMessage(Connector.UOWindowHandle, WM_KEYUP, (int)key, 1);
        }

        public virtual void LClick(int x, int y)
        {
            SendMessage(Connector.UOWindowHandle, WM_SETCURSOR, (int)Connector.UOWindowHandle, (int)MakeLParam(HTCLIENT, WM_MOUSEMOVE));
            SendMessage(Connector.UOWindowHandle, WM_MOUSEMOVE, 0, (int)MakeLParam(x, y));
            SendMessage(Connector.UOWindowHandle, WM_LBUTTONDOWN, MK_LBUTTON, (int)MakeLParam(x, y));
            SendMessage(Connector.UOWindowHandle, WM_LBUTTONUP, MK_LBUTTON, (int)MakeLParam(x, y));
        }

        public virtual void RClick(int x, int y)
        {
            SendMessage(Connector.UOWindowHandle, WM_SETCURSOR, (int)Connector.UOWindowHandle, (int)MakeLParam(HTCLIENT, WM_MOUSEMOVE));
            SendMessage(Connector.UOWindowHandle, WM_MOUSEMOVE, 0, (int)MakeLParam(x, y));
            SendMessage(Connector.UOWindowHandle, WM_RBUTTONDOWN, MK_RBUTTON, (int)MakeLParam(x, y));
            SendMessage(Connector.UOWindowHandle, WM_RBUTTONUP, MK_RBUTTON, (int)MakeLParam(x, y));
        }

        public virtual void LDblClick(int x, int y)
        {
            SendMessage(Connector.UOWindowHandle, WM_SETCURSOR, (int)Connector.UOWindowHandle, (int)MakeLParam(HTCLIENT, WM_MOUSEMOVE));
            SendMessage(Connector.UOWindowHandle, WM_MOUSEMOVE, 0, (int)MakeLParam(x, y));
            SendMessage(Connector.UOWindowHandle, WM_LBUTTONDOWN, MK_LBUTTON, (int)MakeLParam(x, y));
            SendMessage(Connector.UOWindowHandle, WM_LBUTTONUP, MK_LBUTTON, (int)MakeLParam(x, y));
            SendMessage(Connector.UOWindowHandle, WM_LBUTTONDOWN, MK_LBUTTON, (int)MakeLParam(x, y));
            SendMessage(Connector.UOWindowHandle, WM_LBUTTONUP, MK_LBUTTON, (int)MakeLParam(x, y));
        }

        public virtual void RDblClick(int x, int y)
        {
            SendMessage(Connector.UOWindowHandle, WM_SETCURSOR, (int)Connector.UOWindowHandle, (int)MakeLParam(HTCLIENT, WM_MOUSEMOVE));
            SendMessage(Connector.UOWindowHandle, WM_MOUSEMOVE, 0, (int)MakeLParam(x, y));
            SendMessage(Connector.UOWindowHandle, WM_RBUTTONDOWN, MK_RBUTTON, (int)MakeLParam(x, y));
            SendMessage(Connector.UOWindowHandle, WM_RBUTTONUP, MK_RBUTTON, (int)MakeLParam(x, y));
            SendMessage(Connector.UOWindowHandle, WM_RBUTTONDOWN, MK_RBUTTON, (int)MakeLParam(x, y));
            SendMessage(Connector.UOWindowHandle, WM_RBUTTONUP, MK_RBUTTON, (int)MakeLParam(x, y));
        }

        private static IntPtr MakeLParam(int LoWord, int HiWord)
        {
            return (IntPtr)((HiWord << 16) | (LoWord & 0xffff));
        }

        #endregion

        #region Attributes
        public virtual int Life
        {
            get 
            {
                unsafe
                {
                    return *Connector.Attributes.Life;
                }
            }
        }

        public virtual int STR
        {
            get
            {
                unsafe
                {
                    return *Connector.Attributes.STR;
                }
            }
        }

        public virtual int Mana
        {
            get
            {
                unsafe
                {
                    return *Connector.Attributes.Mana;
                }
            }
        }

        public virtual int INT
        {
            get
            {
                unsafe
                {
                    return *Connector.Attributes.INT;
                }
            }
        }

        public virtual int Stamina
        {
            get
            {
                unsafe
                {
                    return *Connector.Attributes.Stamina;
                }
            }
        }

        public virtual int DEX
        {
            get
            {
                unsafe
                {
                    return *Connector.Attributes.DEX;
                }
            }
        }

        public virtual int Armor
        {
            get
            {
                unsafe
                {
                    return *Connector.Attributes.Armor;
                }
            }
        }

        public virtual int Weight
        {
            get
            {
                unsafe
                {
                    return *Connector.Attributes.Weight;
                }
            }
        }

        public virtual int Gold
        {
            get
            {
                unsafe
                {
                    return *Connector.Attributes.Gold;
                }
            }
        }

        public virtual int BM
        {
            get
            {
                unsafe
                {
                    return *Connector.Attributes.BM;
                }
            }
        }

        public virtual int BP
        {
            get
            {
                unsafe
                {
                    return *Connector.Attributes.BP;
                }
            }
        }

        public virtual int GA
        {
            get
            {
                unsafe
                {
                    return *Connector.Attributes.GA;
                }
            }
        }

        public virtual int GS
        {
            get
            {
                unsafe
                {
                    return *Connector.Attributes.GS;
                }
            }
        }

        public virtual int MR
        {
            get
            {
                unsafe
                {
                    return *Connector.Attributes.MR;
                }
            }
        }

        public virtual int NS
        {
            get
            {
                unsafe
                {
                    return *Connector.Attributes.NS;
                }
            }
        }

        public virtual int SA
        {
            get
            {
                unsafe
                {
                    return *Connector.Attributes.SA;
                }
            }
        }

        public virtual int SS
        {
            get
            {
                unsafe
                {
                    return *Connector.Attributes.SS;
                }
            }
        }

        public virtual int VA
        {
            get
            {
                unsafe
                {
                    return *Connector.Attributes.VA;
                }
            }
        }

        public virtual int EN
        {
            get
            {
                unsafe
                {
                    return *Connector.Attributes.EN;
                }
            }
        }

        public virtual int WH
        {
            get
            {
                unsafe
                {
                    return *Connector.Attributes.WH;
                }
            }
        }

        public virtual int FD
        {
            get
            {
                unsafe
                {
                    return *Connector.Attributes.FD;
                }
            }
        }

        public virtual int BR
        {
            get
            {
                unsafe
                {
                    return *Connector.Attributes.BR;
                }
            }
        }

        public virtual int H
        {
            get
            {
                unsafe
                {
                    return *Connector.Attributes.H;
                }
            }
        }

        public virtual int C
        {
            get
            {
                unsafe
                {
                    return *Connector.Attributes.C;
                }
            }
        }

        public virtual int M
        {
            get
            {
                unsafe
                {
                    return *Connector.Attributes.M;
                }
            }
        }

        public virtual int L
        {
            get
            {
                unsafe
                {
                    return *Connector.Attributes.L;
                }
            }
        }

        public virtual int B
        {
            get
            {
                unsafe
                {
                    return *Connector.Attributes.B;
                }
            }
        }

        public virtual int AR
        {
            get
            {
                unsafe
                {
                    return *Connector.Attributes.AR;
                }
            }
        }

        public virtual int BT
        {
            get
            {
                unsafe
                {
                    return *Connector.Attributes.BT;
                }
            }
        }
        #endregion

        #region Click
        public virtual void Click(InjectionConst injConst)
        {
            Connector.ExecFunction("click", new object[] { injConst.ToString() }, 1);
        }

        public virtual void Click(string obj)
        {
            Connector.ExecFunction("click", new object[] { obj }, 1);
        }
        #endregion

        #region FindType
        public virtual void FindType(string type = "-1", string color = "-1", string container = "-1")
        {
            Connector.ExecFunction("findtype", new object[] { type, color, container }, 3);
        }
        #endregion

        #region GetName
        public virtual string GetName(InjectionConst injConst)
        {
            return GetName(injConst.ToString());
        }

        public virtual string GetName(string obj)
        {
            return (string)Connector.ExecFunction("getname", new object[] { obj }, 1);
        }
        #endregion

        #region GetDistance
        public virtual int GetDistance(string obj)
        {
            return (int)(double)Connector.ExecFunction("getdistance", new object[] { obj }, 1);
        }
        #endregion

        #region GetQuantity
        public virtual int GetQuantity(string obj)
        {
            return (int)(double)Connector.ExecFunction("getquantity", new object[] { obj }, 1);
        }
        #endregion

        #region Print
        public virtual void Print(string msg)
        {
            Connector.Print(msg);
        }
        #endregion

        #region IgnoreReset
        public virtual void IgnoreReset()
        {
            Connector.ExecFunction("ignorereset", new object[] { }, 0);
        }
        #endregion

        #region Ignore
        public virtual void Ignore(InjectionConst injConst)
        {
            Ignore(injConst.ToString());
        }        
        public virtual void Ignore(string obj)
        {
            Connector.ExecFunction("ignore", new object[] { obj, "on" }, 2);
        }

        public virtual void Ignore(string obj, bool state)
        {
            Connector.ExecFunction("ignore", new object[] { obj, (state ? "on" : "off") }, 2);
        }
        #endregion

        #region Set
        public virtual void Set(string variable, string value)
        {
            Connector.ExecFunction("set", new object[] { variable, value }, 2);
        }
        #endregion

        #region FindCount
        public virtual int FindCount()
        {
            return (int)(double)Connector.ExecFunction("findcount", new object[] { }, 0);
        }
        #endregion

        #region GetSerial
        public virtual string GetSerial(InjectionConst injConst)
        {
            return GetSerial(injConst.ToString());
        }

        public virtual string GetSerial(string obj)
        {
            return (string)Connector.ExecFunction("getserial", new object[] { obj }, 1);
        }
        #endregion

        #region GetGraphic
        public string GetGraphic(InjectionConst InjConst)
        {
            return GetGraphic(InjConst.ToString());
        }

        public string GetGraphic(string obj)
        {
            return (string)Connector.ExecFunction("getgraphic", new object[] { obj }, 1);
        }
        #endregion

        #region InJournal
        public virtual int InJournal(string msg)
        {
            return (int)(double)Connector.ExecFunction("injournal", new object[] { msg }, 1);
        }
        #endregion

        #region DeleteJournal
        public virtual void DeleteJournal()
        {
            Connector.ExecFunction("deletejournal", new object[] { }, 0);
        }

        public virtual void DeleteJournal(string msg)
        {
            Connector.ExecFunction("deletejournal", new object[] { msg }, 1);
        }
        #endregion

        #region Grab
        public virtual void Grab(InjClasses injConst, int quantity = -1)
        {
            Grab(injConst.ToString(), quantity);
        }

        public virtual void Grab(string obj, int quantity = -1)
        {
            Connector.ExecFunction("grab", new object[] { quantity.ToString(), obj }, 2);
        }
        #endregion

        #region UseObject
        public virtual void UseObject(string obj)
        {
            Connector.ExecFunction("useobject", new object[] { obj }, 1);
        }
        #endregion

        #region ContainerOf
        public virtual string ContainerOf(string obj)
        {
            return (string)Connector.ExecFunction("containerof", new object[] { obj }, 1);
        }
        #endregion

        #region Resend
        public virtual void Resend()
        {
            Connector.ExecFunction("resend", new object[] { }, 0);
        }
        #endregion

        #region MoveItem
        public virtual void MoveItem(string obj, int quantity = -1, string container = "backpack", int x = -1, int y = -1, int z = -1)
        {
            Connector.ExecFunction("moveitem", new object[] { 
                obj, 
                quantity.ToString(), 
                container, 
                x.ToString(), 
                y.ToString(), 
                z.ToString() }, 6);
        }
        #endregion

        #region AddObject
        public virtual void AddObject(string name)
        {
            Connector.ExecFunction("addobject", new object[] { name }, 1);
        }
        #endregion

        #region Targeting
        public virtual bool Targeting()
        {
            return (double)Connector.ExecFunction("targeting", new object[] { }, 0) == 2;
        }
        #endregion

        #region Waiting
        public virtual bool Waiting()
        {
            return (double)Connector.ExecFunction("waiting", new object[] { }, 0) == 1;
        }
        #endregion

        #region GetHP
        public virtual double GetHP(string obj = "my")
        {
            return (double)Connector.ExecFunction("gethp", new object[] { obj }, 1);
        }
        #endregion

        #region GetMaxHP
        public virtual double GetMaxHP(string obj = "my")
        {
            return (double)Connector.ExecFunction("getmaxhp", new object[] { obj }, 1);
        }
        #endregion

        #region SetJournalLine
        public virtual void SetJournalLine(int num, string text)
        {
            Connector.ExecFunction("setjournalline", new object[] { (double)num, text }, 2);
        }
        #endregion

        #region Equip
        public virtual void Equip(InjectionLayer layer, string obj)
        {
            Connector.ExecFunction("equip", new object[] { layer.ToString(), obj }, 2);
        }
        #endregion

        #region EquipT
        public virtual void EquipT(InjectionLayer layer, string type)
        {
            Connector.ExecFunction("equipt", new object[] { layer.ToString(), type }, 2);
        }
        #endregion

        #region Unequip
        public virtual void Unequip(InjectionLayer layer)
        {
            Connector.ExecFunction("unequip", new object[] { layer.ToString() }, 1);
        }
        #endregion

        #region ObjAtLayer
        public virtual string ObjAtLayer(InjectionLayer layer)
        {
            return (string)Connector.ExecFunction("objatlayer", new object[] { layer.ToString() }, 1);
        }
        #endregion

        #region GetLayer
        public virtual InjectionLayer GetLayer(string obj)
        {
            var layer = (string)Connector.ExecFunction("getlayer", new object[] { obj }, 1);
            if (layer == "*")
                return InjectionLayer.None;
            else
                return (InjectionLayer)Enum.Parse(typeof(InjectionLayer), layer);
        }
        #endregion

        #region UseSkill
        public virtual void UseSkill(InjectionSkills skill)
        {
            Connector.ExecFunction("useskill", new object[] { skill.ToString().Replace('_', ' ') }, 1);
        }

        public virtual void UseSkill(InjectionSkills skill, string obj)
        {
            Connector.ExecFunction("useskill", new object[] { skill.ToString().Replace('_', ' '), obj }, 2);
        }
        #endregion

        #region UseType
        public virtual void UseType(string obj, string color)
        {
            Connector.ExecFunction("usetype", new object[] { obj, color }, 2);
        }

        public virtual void UseType(string obj)
        {
            Connector.ExecFunction("usetype", new object[] { obj }, 1);
        }
        #endregion

        #region SkillVal
        public virtual double SkillVal(InjectionSkillVal type)
        {
            return (double)Connector.ExecFunction("skillval", new object[] { (double)(int)type }, 1);
        }
        #endregion

        #region Cast
        public virtual void Cast(InjectionSpell spell)
        {
            Connector.ExecFunction("cast", new object[] { spell.ToString().Replace('_', ' ').Replace("  ", ". ") }, 1);
        }

        public virtual void Cast(InjectionSpell spell, string obj)
        {
            Connector.ExecFunction("cast", new object[] { spell.ToString().Replace('_', ' ').Replace("  ", ". "), obj }, 2);
        }
        #endregion

        #region WaitTargetGround
        public virtual void WaitTargetGround(string type, string color)
        {
            Connector.ExecFunction("waittargetground", new object[] { type, color }, 2);
        }

        public virtual void WaitTargetGround(string type)
        {
            Connector.ExecFunction("waittargetground", new object[] { type }, 1);
        }
        #endregion

        #region WaitTargetLast
        public virtual void WaitTargetLast()
        {
            Connector.ExecFunction("waittargetlast", new object[] { }, 0);
        }
        #endregion

        #region WaitTargetObject
        public virtual void WaitTargetObject(string obj1, string obj2)
        {
            Connector.ExecFunction("waittargetobject", new object[] { obj1, obj2 }, 2);
        }

        public virtual void WaitTargetObject(string obj)
        {
            Connector.ExecFunction("waittargetobject", new object[] { obj }, 1);
        }
        #endregion

        #region WaitTargetObjectType
        public virtual void WaitTargetObjectType(string obj, string type)
        {
            Connector.ExecFunction("waittargetobjecttype", new object[] { obj, type }, 2);
        }

        public virtual void WaitTargetObjectType(string obj, string type, string color)
        {
            Connector.ExecFunction("waittargetobjecttype", new object[] { obj, type, color }, 3);
        }
        #endregion

        #region WaitTargetSelf
        public virtual void WaitTargetSelf()
        {
            Connector.ExecFunction("waittargetself", new object[] { }, 0);
        }
        #endregion

        #region WaitTargetTile
        public virtual void WaitTargetTile(string tile, int x, int y, int z)
        {
            Connector.ExecFunction("waittargettile", new object[] { tile, (double)x, (double)y, (double)z }, 4);
        }
        
        public virtual void WaitTargetTile(string tile)
        {
            Connector.ExecFunction("waittargettile", new object[] { tile }, 1);
        }
        #endregion

        #region WaitTargetType
        public virtual void WaitTargetType(string type)
        {
            Connector.ExecFunction("waittargettype", new object[] { type }, 1);
        }

        public virtual void WaitTargetType(string type, string color)
        {
            Connector.ExecFunction("waittargettype", new object[] { type, color }, 2);
        }
        #endregion

        #region ServerPrint
        public virtual void ServerPrint(string msg)
        {
            Connector.ExecFunction("serverprint", new object[] { msg }, 1);
        }
        #endregion

        #region Msg
        public virtual void Msg(string msg)
        {
            Connector.ExecFunction("msg", new object[] { msg }, 1);
        }
        #endregion

        #region SayU
        public virtual void SayU(string msg)
        {
            Connector.ExecFunction("sayu", new object[] { msg }, 1);
        }
        #endregion

        #region OnMenu
        public virtual void OnMenu()
        {
            Connector.ExecFunction("onmenu", new object[] { }, 0);
        }
        #endregion

        #region AutoMenu
        public virtual void AutoMenu(string prompt, string choice)
        {
            Connector.ExecFunction("automenu", new object[] { prompt, choice }, 2);
        }
        #endregion

        #region CancelMenu
        public virtual void CancelMenu()
        {
            Connector.ExecFunction("cancelmenu", new object[] { }, 0);
        }
        #endregion

        #region LastGump
        public virtual string LastGump(string prop, int num)
        {
            return Connector.ExecFunction("lastgump", new object[] { prop, num }, 2).ToString();
        }

        public virtual string LastGump(string prop)
        {
            return (string)Connector.ExecFunction("lastgump", new object[] { prop }, 1).ToString();
        }
        #endregion

        #region WarMode
        public virtual void WarMode(bool mode)
        {
            Connector.ExecFunction("warmode", new object[] { mode ? (double)1 : (double)0 }, 1);
        }

        public virtual bool WarMode()
        {
            return (double)Connector.ExecFunction("warmode", new object[] { }, 0) == 1;
        }
        #endregion

        #region Attack
        public virtual void Attack(string obj)
        {
            Connector.ExecFunction("attack", new object[] { obj }, 1);
        }
        #endregion

        #region CancelTarget
        public void CancelTarget()
        {
            Connector.ExecFunction("canceltarget", new object[] { }, 0);
        }
        #endregion

        #region GetX
        public virtual int GetX()
        {
            return (int)(double)Connector.ExecFunction("getx", new object[] { }, 0);
        }

        public virtual int GetX(string obj)
        {
            return (int)(double)Connector.ExecFunction("getx", new object[] { obj }, 1);
        }
        #endregion

        #region GetY
        public virtual int GetY()
        {
            return (int)(double)Connector.ExecFunction("gety", new object[] { }, 0);
        }

        public virtual int GetY(string obj)
        {
            return (int)(double)Connector.ExecFunction("gety", new object[] { obj }, 1);
        }
        #endregion

        #region GetZ
        public virtual int GetZ()
        {
            return (int)(double)Connector.ExecFunction("getz", new object[] { }, 0);
        }

        public virtual int GetZ(string obj)
        {
            return (int)(double)Connector.ExecFunction("getz", new object[] { obj }, 1);
        }
        #endregion

        #region Drop
        public virtual void Drop(int quantity, int x, int y, int z, string serial)
        {
            Connector.ExecFunction("drop", new object[] { (double)quantity, (double)x, (double)y, (double)z, serial }, 5);
        }        
        #endregion

        #region DropHere
        public virtual void DropHere(string serial)
        {
            Connector.ExecFunction("drophere", new object[] { serial }, 1);
        }
        #endregion

        #region Exec
        public virtual void Exec(string func)
        {
            Connector.DoCommand(func);            
        }
        #endregion        

        public virtual void MoveToPath(string key, bool reverse)
        {
            var path =  PathEngine.PathEngineForm.LoadPath(key);

            if (reverse)
                path.Coords.Reverse();

            var EUO = InjBundle.GetEUO(this);
            
            foreach (var pt in path.Coords)
            {
                Print(string.Format("Prox. Coord: {0},{1},{2}", pt.X, pt.Y, pt.Z));

                EUO.PathFind(pt.X, pt.Y, pt.Z);

                Point3D lastLoc = Point3D.Zero;
                Point3D currentLoc;
                DateTime lastLocChange = DateTime.Now;

                while (!(currentLoc = Point3D.GetCurrentCoord(this)).Equals(pt))
                {
                    Thread.Sleep(500);

                    if (lastLoc != Point3D.Zero && !lastLoc.Equals(currentLoc))
                        lastLocChange = DateTime.Now;

                    if((DateTime.Now - lastLocChange).TotalSeconds >= 5)
                        break;

                    lastLoc = currentLoc;
                }
                
                Thread.Sleep(500);
            }

            EUO.Close();
        }
    }
}
