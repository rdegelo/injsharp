﻿using InjSharp.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace InjSharp.PathEngine
{
    public partial class PathEngineForm : Form
    {
        InjConnector connector;
        List<FullPath> PathRepository;
        InjClasses UO;
        uoNet.UO EUO;

        public PathEngineForm(InjConnector connector)
        {
            InitializeComponent();

            this.connector = connector;

            UO = new InjClasses(connector);
            EUO = InjBundle.GetEUO(UO);

            PathRepository = LoadPath();

            fullPathBindingSource.DataSource = PathRepository;
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(textBoxName.Text))
            {
                fullPathBindingSource.Add(new FullPath() { Name = textBoxName.Text });
            }
        }

        private void buttonRec_Click(object sender, EventArgs e)
        {
            var currentPath = fullPathBindingSource.Current as FullPath;

            if (buttonRec.Text == "Record")
            {
                buttonRec.Text = "Stop";

                currentPath.Coords = new List<Point3D>();

                timerMonitoraPosicao.Enabled = true;
                timerMonitoraPosicao.Start();
            }
            else 
            {
                buttonRec.Text = "Record";

                timerMonitoraPosicao.Enabled = false;
                timerMonitoraPosicao.Stop();

                currentPath.Coords.Add(Point3D.GetCurrentCoord(UO));

                SavePaths();
            }
        }

        private void buttonVoltar_Click(object sender, EventArgs e)
        {
            var currentPath = fullPathBindingSource.Current as FullPath;

            new Thread(new ThreadStart(() => UO.MoveToPath(currentPath.Name, true))).Start();
        }

        private void timerMonitoraPosicao_Tick(object sender, EventArgs e)
        {
            var currentPath = fullPathBindingSource.Current as FullPath;
            var curCoord = Point3D.GetCurrentCoord(UO);

            if (currentPath.Coords.Count == 0 || !currentPath.Coords.Last().Equals(curCoord))
            {
                currentPath.Coords.Add(curCoord);

                UO.Print(string.Format("Coord Saved: {0},{1},{2}", currentPath.Coords.Last().X, currentPath.Coords.Last().Y, currentPath.Coords.Last().Z));
            }
        }

        public static List<FullPath> LoadPath()
        {
            List<FullPath> result = null;

            try
            {
                using (var stream = new FileStream(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "pathRepository"), FileMode.Open))
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    result = (List<FullPath>)formatter.Deserialize(stream);
                }
            }
            catch (FileNotFoundException)
            {
                result = new List<FullPath>();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return result;
        }

        public static FullPath LoadPath(string key)
        {
            var rep = LoadPath();

            return rep.SingleOrDefault(o => o.Name == key);
        }

        public void SavePaths()
        {
            BinaryFormatter formatter = new BinaryFormatter();
            using (var fs = new FileStream("pathRepository", FileMode.Create))
                formatter.Serialize(fs, PathRepository);
        }
    }
}
