﻿using InjSharp.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InjSharp.PathEngine
{
    [Serializable]
    public class FullPath
    {
        public String Name { get; set; }
        public List<Point3D> Coords { get; set; }
    }
}
