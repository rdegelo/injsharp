﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Runtime.Remoting.Lifetime;
using System.Diagnostics;
using System.Security.Policy;

namespace InjSharp
{
    public partial class MainForm : Form
    {
        private InjConnector connector;

        private Dictionary<string, ScriptsDomain> domains;
        private PathEngine.PathEngineForm pathEngineForm;

        [return: MarshalAs(UnmanagedType.Bool)]
        [DllImport("user32.dll")]
        internal static extern bool GetClientRect(IntPtr hwnd, ref RECT lpRect);

        [StructLayout(LayoutKind.Sequential)]
        internal struct RECT
        {
            internal int left;
            internal int top;
            internal int right;
            internal int bottom;
        }

        public MainForm(InjConnector connector)
        {
            //System.Diagnostics.Debugger.Launch();

            InitializeComponent();

            this.connector = connector;
            domains = new Dictionary<string, ScriptsDomain>();
        }

        #region MapViewer

        private void MainForm_Load(object sender, EventArgs e)
        {
            Ultima.Client.Calibrate();

            RECT rect = new RECT();

            GetClientRect(connector.ParentWindowHandle, ref rect);

            this.Height = rect.bottom - rect.top - 25;
            this.Width = rect.right - rect.left;

            new Thread(new ThreadStart(UpdateMouseCoord)).Start();
        }

        private void OnError(object sender, string ErrorMessage)
        {
            MessageBox.Show(ErrorMessage, "An error occured:", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        #endregion

        private void UpdateMouseCoord()
        {
            while (!this.IsDisposed)
            {
                Point defPnt = new Point();

                NativeMethods.GetCursorPos(ref defPnt);

                this.Invoke(new Action(() => labelMouseCoords.Text = string.Format("X = {0}, Y = {1}", defPnt.X, defPnt.Y)));

                Thread.Sleep(100);
            }
        }

        private void buttonLoad_Click(object sender, EventArgs e)
        {
            openFileDialogScript.AutoUpgradeEnabled = false;
            openFileDialogScript.ShowDialog();

            if (File.Exists(openFileDialogScript.FileName))
            {
                if (domains.ContainsKey(openFileDialogScript.SafeFileName))
                {
                    MessageBox.Show("You can't load a library twice!");
                    return;
                }

                //System.Diagnostics.Debugger.Launch();

                Type proxyType = typeof(DomainProxy);

                AppDomainSetup domaininfo = new AppDomainSetup();
                domaininfo.ApplicationBase = Path.GetDirectoryName(proxyType.Assembly.Location); // System.Environment.CurrentDirectory;
                Evidence adevidence = AppDomain.CurrentDomain.Evidence;

                // Create appdomain
                AppDomain domain = AppDomain.CreateDomain(openFileDialogScript.SafeFileName, adevidence, domaininfo);
                domain.Load(Assembly.GetExecutingAssembly().FullName);
                var currentProxy = (DomainProxy)domain.CreateInstance(proxyType.Assembly.FullName, proxyType.FullName).Unwrap();

                currentProxy.connector = connector;
                currentProxy.ScriptFinished += new EventHandler<ScriptFinishedEventArgs>(currentProxy_ScriptFinished);

                currentProxy.LoadAssembly(openFileDialogScript.FileName);
                var currentDomain = new ScriptsDomain(domain, currentProxy);

                domains.Add(openFileDialogScript.SafeFileName, currentDomain);
                PopulateDomains();

                comboBoxDomains.SelectedItem = currentDomain;
            }
        }

        private void PopulateTypes()
        {
            comboBoxScripts.Items.Clear();
            var domain = comboBoxDomains.SelectedItem as ScriptsDomain;

            if (domain != null)
                comboBoxScripts.Items.AddRange(domain.Proxy.GetTypes());
        }

        private void PopulateDomains()
        {
            comboBoxDomains.Items.Clear();
            comboBoxDomains.Items.AddRange(domains.Values.ToArray());
        }

        void currentProxy_ScriptFinished(object sender, ScriptFinishedEventArgs e)
        {
            this.Invoke(new Action(() =>
            {
                listView1.Items.RemoveByKey(e.Id.ToString());

                var domain = domains.Values.SingleOrDefault(o => o.runningScripts.Contains(e.Id));
                if (domain != null)
                    domain.runningScripts.Remove(e.Id);
            }));
        }

        private void buttonRun_Click(object sender, EventArgs e)
        {
            var selected = comboBoxScripts.SelectedItem as string;
            if (selected != null)
            {
                RunScript(selected);
            }
        }

        public void RunScript(string selected)
        {
            connector.ReloadFunctions();

            ScriptsDomain domain = null;
            foreach (var obj in domains.Values)
                if (obj.Proxy.GetTypes().Contains(selected))
                {
                    domain = obj;
                    break;
                }

            var id = domain.Proxy.RunScript(selected, checkBoxDebug.Checked);
            if (id != String.Empty)
            {
                domain.runningScripts.Add(id);

                listView1.Items.Add(id.ToString(), selected, 0);
            }
        }

        private void buttonUnload_Click(object sender, EventArgs e)
        {
            var domain = comboBoxDomains.SelectedItem as ScriptsDomain;

            if (domain != null)
            {
                if (domain.runningScripts.Count > 0)
                {
                    MessageBox.Show("You can't unload a Domain when it have a script running!");
                    return;
                }

                comboBoxScripts.Items.Clear();

                domains.Remove(domain.ToString());

                AppDomain.Unload(domain.Domain);

                PopulateDomains();
            }
        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 1)
            {
                var domain = domains.Values.SingleOrDefault(o => o.runningScripts.Contains(listView1.SelectedItems[0].Name));
                if (domain != null)
                {
                    domain.Proxy.StopScript(listView1.SelectedItems[0].Name);
                }
            }
        }

        public override object InitializeLifetimeService()
        {
            return null;
        }

        private void comboBoxDomains_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulateTypes();
        }

        private void buttonPathEngine_Click(object sender, EventArgs e)
        {
            pathEngineForm = new PathEngine.PathEngineForm(connector);
            pathEngineForm.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            new Thread(new ThreadStart(() =>
            {
                InjClasses UO = new InjClasses(InjConnector.Instance);
                NativeMethods.SetFocus(InjConnector.Instance.UOWindowHandle);
                UO.LClick(349, 399);
                NativeMethods.SendString("gElo4356");
                Thread.Sleep(5000);
                NativeMethods.SetFocus(InjConnector.Instance.UOWindowHandle);
                UO.LClick(617, 444);
                Thread.Sleep(5000);
                NativeMethods.SetFocus(InjConnector.Instance.UOWindowHandle);
                UO.LClick(617, 444);
                Thread.Sleep(5000);
                NativeMethods.SetFocus(InjConnector.Instance.UOWindowHandle);
                UO.LClick(617, 444);
            })) { IsBackground = true }.Start();
        }
    }

    internal class ScriptsDomain
    {
        public AppDomain Domain { get; private set; }
        public DomainProxy Proxy { get; private set; }
        public List<string> runningScripts;

        public ScriptsDomain(AppDomain domain, DomainProxy proxy)
        {
            this.Domain = domain;
            this.Proxy = proxy;

            runningScripts = new List<string>();
        }

        public override string ToString()
        {
            return Domain.FriendlyName;
        }        
    }

    internal class DomainProxy : MarshalByRefObject
    {
        public Assembly ScriptAssembly { get; private set; }
        public InjConnector connector;

        private Dictionary<string, AsyncData> runningThreads;

        public event EventHandler<ScriptFinishedEventArgs> ScriptFinished;

        public DomainProxy()
        {
            runningThreads = new Dictionary<string, AsyncData>();

            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
            AppDomain.CurrentDomain.FirstChanceException += new EventHandler<System.Runtime.ExceptionServices.FirstChanceExceptionEventArgs>(CurrentDomain_FirstChanceException);
            AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;
        }

        Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            if (args.Name.Contains("resource"))
                return null;

            Debugger.Launch();

            string path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), args.Name + ".dll");

            if (File.Exists(path))
                return Assembly.LoadFrom(path);
            else
                return null;
        }

        void CurrentDomain_FirstChanceException(object sender, System.Runtime.ExceptionServices.FirstChanceExceptionEventArgs e)
        {
            UpdateThreads();
        }

        public override object InitializeLifetimeService()
        {            
            return null;
        }

        private void UpdateThreads()
        {
            var running = runningThreads.ToArray().ToList();

            foreach (var script in running)
            {
                if (script.Value.Thread != null && !script.Value.Thread.IsAlive)
                {
                    if(RemoveScriptFromArray(script.Value.Id.ToString()))
                        connector.Print("There was an error in your script or it has been Aborted!");
                }
            }            
        }

        void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            UpdateThreads();
        }

        public string[] GetTypes()
        {
            return ScriptAssembly.GetTypes().Where(o => o.IsSubclassOf(typeof(InjBundle))).Select(o => o.FullName).ToArray();            
        }

        public void LoadAssembly(string path)
        {
            ScriptAssembly = Assembly.LoadFrom(path);
        }

        public string RunScript(string typeName, bool debug)
        {
            var type = ScriptAssembly.GetTypes().FirstOrDefault(o => o.FullName == typeName);
            if (type != null)
            {
                AsyncData data = new AsyncData();

                data.Script = Activator.CreateInstance(type, connector, debug) as InjBundle;
                data.Id = Guid.NewGuid();

                data.Thread = new Thread(new ParameterizedThreadStart(RunScriptAsync));

                runningThreads.Add(data.Id.ToString(), data);

                data.Thread.Start(data);

                return data.Id.ToString();
            }

            return String.Empty;
        }

        public void StopScript(string id)
        {
            runningThreads[id].Thread.Abort();

            Thread endThread = new Thread(new ParameterizedThreadStart(WaitForThreadEnd));
            endThread.Start(id);
        }

        private void WaitForThreadEnd(object id)
        {
            while (runningThreads[(string)id].Thread.IsAlive)
            {
                Thread.Sleep(500);
            }

            RemoveScriptFromArray(id);
        }

        private bool RemoveScriptFromArray(object id)
        {
            lock (runningThreads)
            {
                if (runningThreads.ContainsKey((string)id))
                {
                    runningThreads.Remove((string)id);

                    if (ScriptFinished != null)
                        ScriptFinished(this, new ScriptFinishedEventArgs((string)id));

                    return true;
                }
            }

            return false;
        }

        private void RunScriptAsync(object p)
        {
            GC.Collect();

            var data = (AsyncData)p;

            data.Script.Start();

            RemoveScriptFromArray(data.Id.ToString());
        }

        private class AsyncData
        {
            public InjBundle Script { get; set; }
            public Guid Id { get; set; }

            public Thread Thread { get; set; }
        }
    }

    [Serializable]
    internal class ScriptFinishedEventArgs : EventArgs
    {
        public string Id { get; private set; }

        public ScriptFinishedEventArgs(string id)
        {
            this.Id = id;
        }
    }
}
