﻿using InjSharp.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InjSharpMapHandler
{
    public class MapHandler : ApplicationContext
    {
        private string m_ID;
        private NamedPipeClientStream m_PipeClientStream;
        
        public MapHandler(string paramID, string surface)
        {
            // Handle the ApplicationExit event to know when the application is exiting.
            Application.ApplicationExit += new EventHandler(Application_ApplicationExit);
            m_ID = paramID;

            surfacePtr = new IntPtr(int.Parse(surface));

            StartIPC();

        }

        void Application_ApplicationExit(object sender, EventArgs e)
        {
            System.Diagnostics.Trace.WriteLine("Application Exit " + m_ID);
        }

        void StartIPC()
        {
            System.Diagnostics.Trace.WriteLine("Starting IPC client for " + m_ID);
            m_PipeClientStream = new NamedPipeClientStream(".",
                                                      m_ID,
                                                      PipeDirection.InOut,
                                                      PipeOptions.Asynchronous);

            try
            {
                m_PipeClientStream.Connect(3000);
                System.Diagnostics.Trace.WriteLine("Connected to IPC server. " + m_ID);

                StreamReader reader = new StreamReader(m_PipeClientStream);

                new Thread(new ThreadStart(UpdateLoop)) { IsBackground = true }.Start();

                //System.Diagnostics.Debugger.Launch();

                while(true)
                {
                    var line = reader.ReadLine();
                    var args = line.Split(' ');

                    CurrentPosition = new Point3D(int.Parse(args[0]), int.Parse(args[1]), int.Parse(args[2]));
                    CurrentMap = int.Parse(args[3]);
                    m_Zoom = float.Parse(args[4]);

                    Thread.Sleep(100);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine("StartIPC Error for " + ex.ToString());

                Process.GetCurrentProcess().Kill();
                return;
            }
        }

        [DllImport("User32.dll")]
        public static extern IntPtr GetDC(IntPtr hwnd);
        [DllImport("User32.dll")]
        public static extern void ReleaseDC(IntPtr hwnd, IntPtr dc);

        [return: MarshalAs(UnmanagedType.Bool)]
        [DllImport("user32.dll")]
        internal static extern bool GetClientRect(IntPtr hwnd, ref RECT lpRect);

        [StructLayout(LayoutKind.Sequential)]
        internal struct RECT
        {
            internal int left;
            internal int top;
            internal int right;
            internal int bottom;
        }

        IntPtr surfacePtr;
        private static Font m_BoldFont = new Font("Courier New", 8, FontStyle.Bold);
        private static Font m_SmallFont = new Font("Arial", 6);
        private static Font m_RegFont = new Font("Arial", 8);
        private Bitmap m_Background;
        int Width, Height;
        Point3D CurrentPosition;
        int CurrentMap;
        float m_Zoom = 1.5f;

        public static bool ComputeMapDetails(Ultima.Map map, int x, int y, out int xCenter, out int yCenter, out int xWidth, out int yHeight)
        {
            xWidth = 5120; yHeight = 4096;

            if (map == Ultima.Map.Trammel || map == Ultima.Map.Felucca)
            {
                if (x >= 0 && y >= 0 && x < 5120 && y < map.Height)
                {
                    xCenter = 1323; yCenter = 1624;
                }
                else if (x >= 5120 && y >= 2304 && x < 6144 && y < map.Height)
                {
                    xCenter = 5936; yCenter = 3112;
                }
                else
                {
                    xCenter = 0; yCenter = 0;
                    return false;
                }
            }
            else if (x >= 0 && y >= 0 && x < map.Width && y < map.Height)
            {
                xCenter = 1323; yCenter = 1624;
            }
            else
            {
                xCenter = map.Width / 2; yCenter = map.Height / 2;
                return false;
            }

            return true;
        }

        public static bool Format(Point p, Ultima.Map map, ref int xLong, ref int yLat, ref int xMins, ref int yMins, ref bool xEast, ref bool ySouth)
        {
            if (map == null)
                return false;

            int x = p.X, y = p.Y;
            int xCenter, yCenter;
            int xWidth, yHeight;

            if (!ComputeMapDetails(map, x, y, out xCenter, out yCenter, out xWidth, out yHeight))
                return false;

            double absLong = (double)((x - xCenter) * 360) / xWidth;
            double absLat = (double)((y - yCenter) * 360) / yHeight;

            if (absLong > 180.0)
                absLong = -180.0 + (absLong % 180.0);

            if (absLat > 180.0)
                absLat = -180.0 + (absLat % 180.0);

            bool east = (absLong >= 0), south = (absLat >= 0);

            if (absLong < 0.0)
                absLong = -absLong;

            if (absLat < 0.0)
                absLat = -absLat;

            xLong = (int)absLong;
            yLat = (int)absLat;

            xMins = (int)((absLong % 1.0) * 60);
            yMins = (int)((absLat % 1.0) * 60);

            xEast = east;
            ySouth = south;

            return true;
        }

        private void UpdateLoop()
        {
            try
            {
                while (true)
                {
                    FullUpdate();

                    Thread.Sleep(100);
                }
            }
            catch (Exception)
            {                
            }
        }

        public void FullUpdate()
        {
            RECT rect = new RECT();
            GetClientRect(surfacePtr, ref rect);

            this.Width = rect.right - rect.left;
            this.Height = rect.bottom - rect.top;

            if (this.Width == 0 && this.Height == 0)
                return;

            if (m_Background != null)
                m_Background.Dispose();

            Ultima.Map map = InjSharp.Core.Map.GetMap(this.CurrentMap);
            if (map == null)
                map = Ultima.Map.Felucca;

            if (GC.GetTotalMemory(true) > 200000000)
                Ultima.Map.Reload();

            //Debug.WriteLine("Memory: " + Process.GetCurrentProcess().VirtualMemorySize64);

            m_Background = new Bitmap(this.Width, this.Height);

            int xLong = 0, yLat = 0;
            int xMins = 0, yMins = 0;
            bool xEast = false, ySouth = false;

            int w = (this.Width) >> 3;
            int h = (this.Height) >> 3;
            int xtrans = this.Width / 2;
            int ytrans = this.Height / 2;
            Point3D focus = CurrentPosition;
            Point offset = new Point(focus.X & 7, focus.Y & 7);
            Point mapOrigin = new Point((focus.X >> 3) - (w / 2), (focus.Y >> 3) - (h / 2));
            Point pntPlayer = new Point((focus.X) - (mapOrigin.X << 3) - offset.X, (focus.Y) - (mapOrigin.Y << 3) - offset.Y);

            Graphics gfx = Graphics.FromImage(m_Background);

            gfx.FillRectangle(Brushes.Black, 0, 0, this.Width, this.Height);

            gfx.TranslateTransform(-xtrans, -ytrans, MatrixOrder.Append);
            gfx.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
            gfx.PageUnit = GraphicsUnit.Pixel;
            gfx.ScaleTransform(m_Zoom, m_Zoom, MatrixOrder.Append); //Aqui que implementa o Zoom!!
            gfx.RotateTransform(45, MatrixOrder.Append);
            gfx.TranslateTransform(xtrans, ytrans, MatrixOrder.Append);

            gfx.DrawImage(map.GetImage(mapOrigin.X, mapOrigin.Y, w + offset.X, h + offset.Y, true), -offset.X, -offset.Y);

            gfx.ScaleTransform(1.0f, 1.0f, MatrixOrder.Append);

            //ArrayList regions = new ArrayList();
            //ArrayList mButtons = new ArrayList();
            //if (this.Width > this.Height)
            //{
            //    regions = RegionList(focus.X, focus.Y, this.Width);
            //    mButtons = ButtonList(focus.X, focus.Y, this.Width);
            //}
            //else
            //{
            //    regions = RegionList(focus.X, focus.Y, this.Height);
            //    mButtons = ButtonList(focus.X, focus.Y, this.Height);
            //}

            //foreach (Region region in regions)
            //    gfx.DrawRectangle(Pens.LimeGreen, (region.X) - ((mapOrigin.X << 3) + offset.X), (region.Y) - ((mapOrigin.Y << 3) + offset.Y), region.Width, region.Length);

            gfx.DrawLine(Pens.Silver, pntPlayer.X - 2, pntPlayer.Y - 2, pntPlayer.X + 2, pntPlayer.Y + 2);
            gfx.DrawLine(Pens.Silver, pntPlayer.X - 2, pntPlayer.Y + 2, pntPlayer.X + 2, pntPlayer.Y - 2);
            gfx.FillRectangle(Brushes.Red, pntPlayer.X, pntPlayer.Y, 1, 1);
            //gfx.DrawEllipse( Pens.Silver, pntPlayer.X - 2, pntPlayer.Y - 2, 4, 4 );

            gfx.DrawString("W", m_BoldFont, Brushes.Red, pntPlayer.X - 35, pntPlayer.Y - 5);
            gfx.DrawString("E", m_BoldFont, Brushes.Red, pntPlayer.X + 25, pntPlayer.Y - 5);
            gfx.DrawString("N", m_BoldFont, Brushes.Red, pntPlayer.X - 5, pntPlayer.Y - 35);
            gfx.DrawString("S", m_BoldFont, Brushes.Red, pntPlayer.X - 5, pntPlayer.Y + 25);

            gfx.ResetTransform();


            if (Format(new Point(focus.X, focus.Y), Ultima.Map.Felucca, ref xLong, ref yLat, ref xMins, ref yMins, ref xEast, ref ySouth))
            {
                string locString = String.Format("{0}°{1}'{2} {3}°{4}'{5} | ({6},{7})", yLat, yMins, ySouth ? "S" : "N", xLong, xMins, xEast ? "E" : "W", CurrentPosition.X, CurrentPosition.Y);
                SizeF size = gfx.MeasureString(locString, m_RegFont);
                gfx.FillRectangle(Brushes.Wheat, 0, 0, size.Width + 2, size.Height + 2);
                gfx.DrawRectangle(Pens.Black, 0, 0, size.Width + 2, size.Height + 2);
                gfx.DrawString(locString, m_RegFont, Brushes.Black, 1, 1);
            }

            gfx.ResetTransform();
            gfx.Dispose();

            Paint();
        }

        private void Paint()
        {
            try
            {

                IntPtr desktopPtr = GetDC(surfacePtr);
                Graphics gfx = Graphics.FromHdc(desktopPtr);

                gfx.DrawImageUnscaled(m_Background, 0, 0);

                int w = (this.Width) >> 3;
                int h = (this.Height) >> 3;
                int xtrans = this.Width / 2;
                int ytrans = this.Height / 2;
                Point3D focus = CurrentPosition;
                Point offset = new Point(focus.X & 7, focus.Y & 7);
                Point mapOrigin = new Point((focus.X >> 3) - (w / 2), (focus.Y >> 3) - (h / 2));

                gfx.TranslateTransform(-xtrans, -ytrans, MatrixOrder.Append);
                gfx.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
                gfx.PageUnit = GraphicsUnit.Pixel;
                gfx.ScaleTransform(m_Zoom, m_Zoom, MatrixOrder.Append);
                gfx.RotateTransform(45, MatrixOrder.Append);
                gfx.TranslateTransform(xtrans, ytrans, MatrixOrder.Append);
                gfx.ScaleTransform(1.0f, 1.0f, MatrixOrder.Append);
                gfx.ResetTransform();

                gfx.Dispose();
                ReleaseDC(IntPtr.Zero, desktopPtr);
            }
            catch { }
        }
    }
}
